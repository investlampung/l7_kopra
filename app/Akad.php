<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Akad extends Model
{
    protected $guarded = [];
    protected $dates = ['tanggal_janji','pasal24_a','created_at'];

    public function anggota()
    {
        return $this->belongsTo('App\Anggota', 'anggota_id');
    }
    public function akadbarang()
    {
        return $this->hasMany('App\AkadBarang');
    }
}
