<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Angsuran extends Model
{
    protected $guarded = [];
    public function anggota()
    {
        return $this->belongsTo('App\Anggota', 'anggota_id');
    }
    public function bulan()
    {
        return $this->belongsTo('App\Bulan', 'bulan_id');
    }
}
