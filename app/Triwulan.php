<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Triwulan extends Model
{
    protected $guarded = [];
    public function kelompok()
    {
        return $this->belongsTo('App\Kelompok', 'kelompok_id');
    }
}
