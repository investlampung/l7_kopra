<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pekerjaan extends Model
{
    protected $guarded = [];

    public function anggota()
    {
        return $this->hasMany('App\Anggota');
    }
}
