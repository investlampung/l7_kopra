<?php

namespace App\Imports;

use App\AkadBarang;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportJpa implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        return new AkadBarang([
            'akad_id'     => $row['akad_id'],
            'tanggal'     => $this->transformDate($row['tanggal']),
            'jml'         => $row['jml'],
        ]);
    }

    public function transformDate($value, $format = 'Y-m-d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }
}
