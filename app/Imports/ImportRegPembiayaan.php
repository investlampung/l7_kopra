<?php

namespace App\Imports;

use App\Anggota;
use App\Angsuran;
use App\Bulan;
use App\Tahun;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportRegPembiayaan implements ToModel, WithHeadingRow
{
    private $data;

    public function __construct(string $data)
    {
        $this->data = $data;
    }

    public function model(array $row)
    {
        if (empty($this->anggota($row['nonasabah']))) {
        } else {
            $anggota_id = $this->anggota($row['nonasabah']);
            $bulan_id = $this->data;
            $ang = Angsuran::where('anggota_id', $anggota_id)->where('bulan_id', $bulan_id)->get()->first();
            if (empty($ang)) {
                return new Angsuran([
                    'anggota_id'     => $this->anggota($row['nonasabah']),
                    // 'bulan_id'     => $this->bulan(date('Y')),
                    'bulan_id'     => $this->data,
                    'dana'         => $row['cilpokok'],
                ]);
            } else {
            }
        }
    }

    public function anggota($item)
    {
        $ang = Anggota::where('kode_pembiayaan', $item)->get();
        foreach ($ang as $key => $user) {
            $anggota = $user['id'];
            return $anggota;
        }
        // dd($anggota);
    }

    public function bulan($tahun)
    {
        $tahun = date('Y');
        $id_tahun = Tahun::where('tahun', $tahun)->get()->first();

        $bulan = date('m');

        if ($bulan == '01') {
            $bulan = 'Januari';
        } else if ($bulan == '02') {
            $bulan = 'Februari';
        } else if ($bulan == '03') {
            $bulan = 'Maret';
        } else if ($bulan == '04') {
            $bulan = 'April';
        } else if ($bulan == '05') {
            $bulan = 'Mei';
        } else if ($bulan == '06') {
            $bulan = 'Juni';
        } else if ($bulan == '07') {
            $bulan = 'Juli';
        } else if ($bulan == '08') {
            $bulan = 'Agustus';
        } else if ($bulan == '09') {
            $bulan = 'September';
        } else if ($bulan == '10') {
            $bulan = 'Oktober';
        } else if ($bulan == '11') {
            $bulan = 'November';
        } else if ($bulan == '12') {
            $bulan = 'Desember';
        } else {
        }

        $id_bulan = Bulan::where('tahun_id', $id_tahun->id)->where('bulan', $bulan)->get()->first();
        // dd($bulan);
        return $id_bulan->id;
    }
}
