<?php

namespace App\Imports;

use App\Anggota;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportAnggota implements ToModel, WithHeadingRow
{

    public function model(array $row)
    {
        $rek = $row['kode_rekening'];
        $pem = $row['kode_pembiayaan'];
        $data = Anggota::where('kode_rekening', $rek)->where('kode_pembiayaan', $pem)->get()->first();
        if (empty($data)) {
            return new Anggota([
                'kelompok_id'       => $row['kelompok_id'],
                'pekerjaan_id'      => $row['pekerjaan_id'],
                'nama'              => $row['nama'],
                'kode_rekening'     => $row['kode_rekening'],
                'kode_pembiayaan'   => $row['kode_pembiayaan'],
                'alamat'            => $row['alamat'],
                'no_ktp'            => $row['no_ktp'],
                'no_telp'           => $row['no_telp'],
                'ket'               => $row['ket'],
            ]);
        } else {
        }
    }
}
