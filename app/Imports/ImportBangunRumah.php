<?php

namespace App\Imports;

use App\BangunRumah;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportBangunRumah implements ToModel, WithHeadingRow
{

    public function model(array $row)
    {
        $anggota_id = $row['anggota_id'];
        $data = BangunRumah::where('anggota_id', $anggota_id)->get()->first();
        if (empty($data)) {
            return new BangunRumah([
                'tahun_id'       => $row['tahun_id'],
                'anggota_id'      => $row['anggota_id'],
                'tw'              => $row['tw'],
            ]);
        } else {
        }
    }
}
