<?php

namespace App\Imports;

use App\Akad;
use App\AkadBarang;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportAkad implements ToModel, WithHeadingRow
{

    public function model(array $row)
    {
        return new Akad([
            'anggota_id'        => $row['anggota_id'],
            'no_janji'          => $row['no_janji'],
            'no_syariah'        => $row['no_syariah'],
            'tanggal_janji'     => $this->transformDate($row['tanggal_janji']),
            'pasal1_a'          => $row['pasal1_a'],
            'pasal1_b'          => $row['pasal1_b'],
            'pasal1_c'          => $row['pasal1_c'],
            'pasal1_d'          => $row['pasal1_d'],
            'pasal1_e'          => $row['pasal1_e'],
            'pasal1_f'          => $row['pasal1_f'],
            'pasal1_g'          => $row['pasal1_g'],
            'pasal1_h'          => $row['pasal1_h'],
            'pasal1_i'          => $row['pasal1_i'],
            'pasal1_j'          => $row['pasal1_j'],
            'pasal1_k'          => $row['pasal1_k'],
            'pasal1_l'          => $row['pasal1_l'],
            'pasal5_a'          => $row['pasal5_a'],
            'pasal5_b'          => $row['pasal5_b'],
            'pasal5_c'          => $row['pasal5_c'],
            'pasal5_d'          => $row['pasal5_d'],
            'pasal8_a'          => $row['pasal8_a'],
            'pasal24_a'         => $this->transformDate($row['pasal24_a']),
            'lamp_ang_a'        => $row['lamp_ang_a'],
            'lamp_ang_b'        => $row['lamp_ang_b'],
            'lamp_ang_c'        => $row['lamp_ang_c'],
            'lamp_ang_d'        => $row['lamp_ang_d'],
            'suplier_nama'      => $row['suplier_nama'],
            'suplier_alamat'    => $row['suplier_alamat'],
        ]);
    }

    public function transformDate($value, $format = 'Y-m-d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }
}
