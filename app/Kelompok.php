<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelompok extends Model
{
    protected $guarded = [];

    public function anggota()
    {
        return $this->hasMany('App\Anggota');
    }

    public function triwulan()
    {
        return $this->hasMany('App\Triwulan');
    }
}
