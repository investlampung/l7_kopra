<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anggota extends Model
{
    protected $guarded = [];

    public function kelompok()
    {
        return $this->belongsTo('App\Kelompok', 'kelompok_id');
    }
    public function pekerjaan()
    {
        return $this->belongsTo('App\Pekerjaan', 'pekerjaan_id');
    }

    public function angsuran()
    {
        return $this->hasMany('App\Angsuran');
    }
    public function bangun()
    {
        return $this->hasMany('App\BangunRumah');
    }

    public function akad()
    {
        return $this->hasMany('App\Akad');
    }
}
