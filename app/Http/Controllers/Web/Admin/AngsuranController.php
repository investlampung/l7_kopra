<?php

namespace App\Http\Controllers\Web\Admin;

use App\Anggota;
use App\Angsuran;
use App\Bulan;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\AngsuranRequest;
use App\Kelompok;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class AngsuranController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no = 1;
        $tahun_id = Cookie::get('id_tahun');
        $bulan = Bulan::where('tahun_id',$tahun_id)->orderBy('id', 'ASC')->get()->all();
        $data = Kelompok::orderBy('kelompok', 'ASC')->get()->all();
        return view('admin.angsuran.beranda', compact('data', 'no','bulan'));
    }

    public function create()
    {
        //
    }

    public function store(AngsuranRequest $request)
    {
        $data = $request->all();
        Angsuran::create($data);

        $anggota = Anggota::where('id', $request->anggota_id)->get()->first();

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Angsuran atas nama " . $anggota->nama;
        History::create($histori);

        $notification = array(
            'message' => 'Data Angsuran atas nama "' . $anggota->nama . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.angsuran.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $tahun_id = Cookie::get('id_tahun');
        $bulan = Bulan::where('tahun_id',$tahun_id)->orderBy('id', 'ASC')->get()->all();
        $kelompok = Kelompok::orderBy('kelompok', 'ASC')->get()->all();
        $data = Angsuran::findOrFail($id);
        return view('admin.angsuran.edit', compact('data','kelompok','bulan'));
    }

    public function update(AngsuranRequest $request, $id)
    {
        $data = Angsuran::findOrFail($id);
        
        $anggota = Anggota::where('id', $data->anggota_id)->get()->first();

        $data->update($request->all());


        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Angsuran atas nama " . $anggota->nama;
        History::create($histori);

        $notification = array(
            'message' => 'Data Angsuran atas nama "' . $anggota->nama . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.angsuran.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = Angsuran::findOrFail($id);

        $anggota = Anggota::where('id', $data->anggota_id)->get()->first();

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Angsuran atas nama " . $anggota->nama;
        History::create($histori);

        $notification = array(
            'message' => 'Data Angsuran atas nama "' . $anggota->nama . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.angsuran.index')->with($notification);
    }
}
