<?php

namespace App\Http\Controllers\Web\Admin;

use App\Barang;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\BarangRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BarangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no = 1;
        $data = Barang::orderBy('barang', 'ASC')->get()->all();
        return view('admin.barang.beranda', compact('data', 'no'));
    }

    public function create()
    {
        //
    }

    public function store(BarangRequest $request)
    {
        $data = $request->all();
        Barang::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Barang " . $request->barang;
        History::create($histori);

        $notification = array(
            'message' => 'Data Barang "' . $request->barang . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.barang.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Barang::findOrFail($id);
        return view('admin.barang.edit', compact('data'));
    }

    public function update(BarangRequest $request, $id)
    {
        $data = Barang::findOrFail($id);
        $data->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Barang " . $request->barang;
        History::create($histori);

        $notification = array(
            'message' => 'Data Barang "' . $request->barang . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.barang.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = Barang::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Barang " . $data->barang;
        History::create($histori);

        $notification = array(
            'message' => 'Data Barang "' . $data->barang . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.barang.index')->with($notification);
    }
}
