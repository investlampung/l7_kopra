<?php

namespace App\Http\Controllers\Web\Admin;

use App\BangunRumah;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\BangunRequest;
use App\Kelompok;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class BangunRumahController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no = 1;
        $kelompok = Kelompok::orderBy('kelompok', 'ASC')->get()->all();
        $tahun_id = Cookie::get('id_tahun');
        $data = BangunRumah::where('tahun_id', $tahun_id)->orderBy('tw', 'ASC')->get()->all();
        return view('admin.bangun.beranda', compact('data', 'no', 'kelompok'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $tahun_id = Cookie::get('id_tahun');
        $data = $request->all();
        $data['tahun_id'] = $tahun_id;
        BangunRumah::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Bangun Rumah";
        History::create($histori);

        $notification = array(
            'message' => 'Data Bangun Rumah berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.bangunrumah.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $kelompok = Kelompok::orderBy('kelompok', 'ASC')->get()->all();
        $data = BangunRumah::findOrFail($id);
        return view('admin.bangun.edit', compact('data', 'kelompok'));
    }

    public function update(Request $request, $id)
    {
        $tahun_id = Cookie::get('id_tahun');
        $dataa = BangunRumah::findOrFail($id);
        $data = $request->all();
        $data['tahun_id'] = $tahun_id;
        $dataa->update($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Bangun Rumah";
        History::create($histori);

        $notification = array(
            'message' => 'Data Bangun Rumah berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.bangunrumah.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = BangunRumah::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Bangun Rumah";
        History::create($histori);

        $notification = array(
            'message' => 'Data Bangun Rumah berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.bangunrumah.index')->with($notification);
    }
}
