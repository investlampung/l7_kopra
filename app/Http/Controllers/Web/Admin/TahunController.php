<?php

namespace App\Http\Controllers\Web\Admin;

use App\Bulan;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\TahunRequest;
use App\Tahun;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TahunController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no = 1;
        $data = Tahun::orderBy('tahun', 'ASC')->get()->all();
        return view('admin.tahun.beranda', compact('data', 'no'));
    }

    public function create()
    {
        //
    }

    public function store(TahunRequest $request)
    {
        $data = $request->all();
        Tahun::create($data);

        $tahun = Tahun::orderBy('id', 'DESC')->get()->first();
        Bulan::create(['tahun_id' => $tahun->id, 'bulan' => 'Januari']);
        Bulan::create(['tahun_id' => $tahun->id, 'bulan' => 'Februari']);
        Bulan::create(['tahun_id' => $tahun->id, 'bulan' => 'Maret']);
        Bulan::create(['tahun_id' => $tahun->id, 'bulan' => 'April']);
        Bulan::create(['tahun_id' => $tahun->id, 'bulan' => 'Mei']);
        Bulan::create(['tahun_id' => $tahun->id, 'bulan' => 'Juni']);
        Bulan::create(['tahun_id' => $tahun->id, 'bulan' => 'Juli']);
        Bulan::create(['tahun_id' => $tahun->id, 'bulan' => 'Agustus']);
        Bulan::create(['tahun_id' => $tahun->id, 'bulan' => 'September']);
        Bulan::create(['tahun_id' => $tahun->id, 'bulan' => 'Oktober']);
        Bulan::create(['tahun_id' => $tahun->id, 'bulan' => 'November']);
        Bulan::create(['tahun_id' => $tahun->id, 'bulan' => 'Desember']);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Tahun " . $request->tahun;
        History::create($histori);

        $notification = array(
            'message' => 'Data Tahun "' . $request->tahun . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.tahun.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Tahun::findOrFail($id);
        return view('admin.tahun.edit', compact('data'));
    }

    public function update(TahunRequest $request, $id)
    {
        $data = Tahun::findOrFail($id);
        $data->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Tahun " . $request->tahun;
        History::create($histori);

        $notification = array(
            'message' => 'Data Tahun "' . $request->tahun . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.tahun.index')->with($notification);
    }

    public function destroy($id)
    {

        $data = Tahun::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Tahun " . $data->tahun;
        History::create($histori);

        $notification = array(
            'message' => 'Data Tahun "' . $data->tahun . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        Bulan::where('tahun_id', $data->id)->delete();
        $data->delete();

        return redirect()->route('admin.tahun.index')->with($notification);
    }
}
