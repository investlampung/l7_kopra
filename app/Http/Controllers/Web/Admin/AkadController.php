<?php

namespace App\Http\Controllers\Web\Admin;

use App\Akad;
use App\AkadBarang;
use App\Anggota;
use App\Barang;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\AkadRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AkadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no = 1;
        $data = Akad::orderBy('id', 'DESC')->get()->all();
        return view('admin.akad.beranda', compact('data', 'no'));
    }

    public function dataAjax(Request $request)
    {
        $data = [];
        if ($request->has('q')) {
            $search = $request->q;
            $data = DB::table("anggotas")
                ->select("id", "nama")
                ->where('nama', 'LIKE', "%$search%")
                ->get();
        }


        return response()->json($data);
    }

    public function create()
    {
        $anggota = Anggota::orderBy('nama', 'ASC')->get()->all();
        return view('admin.akad.create', compact('anggota'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        Akad::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Akad.";
        History::create($histori);

        $notification = array(
            'message' => 'Data Akad berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.akad.index')->with($notification);
    }

    public function show($id)
    {
        $no = 1;
        $data = Akad::findOrFail($id);
        $barang = AkadBarang::where('akad_id', $id)->get()->all();
        return view('admin.akad.show', compact('data', 'barang', 'no'));
    }

    public function edit($id)
    {
        $anggota = Anggota::orderBy('nama', 'ASC')->get()->all();
        $data = Akad::findOrFail($id);
        return view('admin.akad.edit', compact('data', 'anggota'));
    }

    public function update(Request $request, $id)
    {
        $data = Akad::findOrFail($id);
        $data->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Akad.";
        History::create($histori);

        $notification = array(
            'message' => 'Data Akad berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.akad.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = Akad::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Akad.";
        History::create($histori);

        $notification = array(
            'message' => 'Data Akad berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.akad.index')->with($notification);
    }
}
