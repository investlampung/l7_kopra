<?php

namespace App\Http\Controllers\Web\Admin;

use App\BangunRumah;
use App\Bulan;
use App\Http\Controllers\Controller;
use App\Kelompok;
use App\Triwulan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class TriwulanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no = 1;
        $data = Kelompok::where('kelompok', 'NOT LIKE', '%Tahap%')->orderBy('kelompok', 'ASC')->get()->all();
        return view('admin.triwulan.beranda', compact('data', 'no'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
    }

    public function show($id)
    {
        $no = 1;
        $data = Kelompok::findOrFail($id);
        $tahun_id = Cookie::get('id_tahun');
        $bulan = Bulan::where('tahun_id', $tahun_id)->orderBy('id', 'ASC')->get()->all();
        return view('admin.triwulan.show', compact('data', 'no', 'bulan'));
    }

    public function show_triwulan($id, $tw)
    {
        $tahun_id = Cookie::get('id_tahun');
        $tahun = Cookie::get('tahun');
        $bulan = Bulan::where('tahun_id', $tahun_id)->orderBy('id', 'ASC')->get()->all();
        $bangun_rumah = BangunRumah::where('tahun_id', $tahun_id)->where('tw', $tw)->get()->first();
        // dd($bangun_rumah);
        $bangun = '';
        if (empty($bangun_rumah)) {
        } else {
            $bangun = $bangun_rumah->anggota->nama;
        }
        $no = 1;
        $data = Kelompok::findOrFail($id);

        $bulan1 = '';
        $bulan2 = '';
        $bulan3 = '';
        $bulan_nama1 = '';
        $bulan_nama2 = '';
        $bulan_nama3 = '';

        if ($tw == 'tw1') {
            foreach ($bulan as $bul) {
                if ($bul->bulan === 'Januari') {
                    $bulan1 = $bul->id;
                    $bulan_nama1 = $bul->bulan;
                } else if ($bul->bulan === 'Februari') {
                    $bulan2 = $bul->id;
                    $bulan_nama2 = $bul->bulan;
                } else if ($bul->bulan === 'Maret') {
                    $bulan3 = $bul->id;
                    $bulan_nama3 = $bul->bulan;
                } else {
                }
            }
        } else if ($tw == 'tw2') {
            foreach ($bulan as $bul) {
                if ($bul->bulan === 'April') {
                    $bulan1 = $bul->id;
                    $bulan_nama1 = $bul->bulan;
                } else if ($bul->bulan === 'Mei') {
                    $bulan2 = $bul->id;
                    $bulan_nama2 = $bul->bulan;
                } else if ($bul->bulan === 'Juni') {
                    $bulan3 = $bul->id;
                    $bulan_nama3 = $bul->bulan;
                } else {
                }
            }
        } else if ($tw == 'tw3') {
            foreach ($bulan as $bul) {
                if ($bul->bulan === 'Juli') {
                    $bulan1 = $bul->id;
                    $bulan_nama1 = $bul->bulan;
                } else if ($bul->bulan === 'Agustus') {
                    $bulan2 = $bul->id;
                    $bulan_nama2 = $bul->bulan;
                } else if ($bul->bulan === 'September') {
                    $bulan3 = $bul->id;
                    $bulan_nama3 = $bul->bulan;
                } else {
                }
            }
        } else {
            foreach ($bulan as $bul) {
                if ($bul->bulan === 'Oktober') {
                    $bulan1 = $bul->id;
                    $bulan_nama1 = $bul->bulan;
                } else if ($bul->bulan === 'November') {
                    $bulan2 = $bul->id;
                    $bulan_nama2 = $bul->bulan;
                } else if ($bul->bulan === 'Desember') {
                    $bulan3 = $bul->id;
                    $bulan_nama3 = $bul->bulan;
                } else {
                }
            }
        }

        // dd($bulan1);
        return view('admin.triwulan.show', compact('data', 'no', 'tahun', 'bangun', 'bulan1', 'bulan2', 'bulan3', 'tw', 'bulan_nama1', 'bulan_nama2', 'bulan_nama3'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
