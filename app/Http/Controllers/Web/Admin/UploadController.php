<?php

namespace App\Http\Controllers\Web\Admin;

use App\Bulan;
use App\History;
use App\Http\Controllers\Controller;
use App\Imports\ImportAkad;
use App\Imports\ImportAnggota;
use App\Imports\ImportBangunRumah;
use App\Imports\ImportJpa;
use App\Imports\ImportRegPembiayaan;
use App\Imports\ImportRegSimpanan;
use Illuminate\Http\Request;

use Excel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class UploadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function anggota()
    {
        return view('admin.upload.anggota');
    }

    public function import_anggota(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file'); //GET FILE
            Excel::import(new ImportAnggota, $file); //IMPORT FILE 

            // HISTORI
            $histori['user'] = Auth::user()->name;
            $histori['info'] = "Tambah";
            $histori['desc_info'] = "Upload Data Anggota.";
            History::create($histori);

            $notification = array(
                'message' => 'Upload Data Anggota berhasil.',
                'alert-type' => 'info'
            );
            return redirect()->route('admin.anggota.index')->with($notification);
        }
    }

    public function akad()
    {
        return view('admin.upload.akad');
    }

    public function import_akad(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file'); //GET FILE
            Excel::import(new ImportAkad, $file); //IMPORT FILE 

            // HISTORI
            $histori['user'] = Auth::user()->name;
            $histori['info'] = "Tambah";
            $histori['desc_info'] = "Upload Data Akad.";
            History::create($histori);

            $notification = array(
                'message' => 'Upload Data Akad berhasil.',
                'alert-type' => 'info'
            );
            return redirect()->route('admin.akad.index')->with($notification);
        }
    }

    public function jpa()
    {
        return view('admin.upload.jpa');
    }

    public function import_jpa(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file'); //GET FILE
            Excel::import(new ImportJpa, $file); //IMPORT FILE 

            // HISTORI
            $histori['user'] = Auth::user()->name;
            $histori['info'] = "Tambah";
            $histori['desc_info'] = "Upload Data Jadwal Pembayaran Angsuran.";
            History::create($histori);

            $notification = array(
                'message' => 'Upload Data Jadwal Pembayaran Angsuran.',
                'alert-type' => 'info'
            );
            return redirect()->route('admin.akad.index')->with($notification);
        }
    }

    public function regsim()
    {
        $tahun_id = Cookie::get('id_tahun');
        $bulan = Bulan::where('tahun_id', $tahun_id)->orderBy('id', 'ASC')->get()->all();
        return view('admin.upload.regsim', compact('bulan'));
    }

    public function import_regsim(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file'); //GET FILE
            $bulan_id = $request->bulan_id; //GET FILE
            Excel::import(new ImportRegSimpanan($bulan_id), $file); //IMPORT FILE 

            // HISTORI
            $histori['user'] = Auth::user()->name;
            $histori['info'] = "Tambah";
            $histori['desc_info'] = "Upload Data Registrasi Simpanan.";
            History::create($histori);

            $notification = array(
                'message' => 'Upload Data Registrasi Simpanan.',
                'alert-type' => 'info'
            );

            return redirect()->route('admin.angsuran.index')->with($notification);
        }
    }

    public function regpem()
    {
        $tahun_id = Cookie::get('id_tahun');
        $bulan = Bulan::where('tahun_id', $tahun_id)->orderBy('id', 'ASC')->get()->all();
        return view('admin.upload.regpem', compact('bulan'));
    }

    public function import_regpem(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file'); //GET FILE
            $bulan_id = $request->bulan_id; //GET FILE
            Excel::import(new ImportRegPembiayaan($bulan_id), $file); //IMPORT FILE 

            // HISTORI
            $histori['user'] = Auth::user()->name;
            $histori['info'] = "Tambah";
            $histori['desc_info'] = "Upload Data Registrasi Pembiayaan.";
            History::create($histori);

            $notification = array(
                'message' => 'Upload Data Registrasi Pembiayaan.',
                'alert-type' => 'info'
            );

            return redirect()->route('admin.angsuran.index')->with($notification);
        }
    }

    public function bangunrumah()
    {
        return view('admin.upload.bangunrumah');
    }

    public function import_bangunrumah(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file'); //GET FILE
            Excel::import(new ImportBangunRumah, $file); //IMPORT FILE 

            // HISTORI
            $histori['user'] = Auth::user()->name;
            $histori['info'] = "Tambah";
            $histori['desc_info'] = "Upload Data Bangun Rumah.";
            History::create($histori);

            $notification = array(
                'message' => 'Upload Data Bangun Rumah.',
                'alert-type' => 'info'
            );

            return redirect()->route('admin.bangunrumah.index')->with($notification);
        }
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
