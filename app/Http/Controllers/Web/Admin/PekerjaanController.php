<?php

namespace App\Http\Controllers\Web\Admin;

use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\PekerjaanRequest;
use App\Pekerjaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PekerjaanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no = 1;
        $data = Pekerjaan::orderBy('pekerjaan', 'ASC')->get()->all();
        return view('admin.pekerjaan.beranda', compact('data', 'no'));
    }

    public function create()
    {
        //
    }

    public function store(PekerjaanRequest $request)
    {
        $data = $request->all();
        Pekerjaan::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Pekerjaan " . $request->pekerjaan;
        History::create($histori);

        $notification = array(
            'message' => 'Data Pekerjaan "' . $request->pekerjaan . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.pekerjaan.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Pekerjaan::findOrFail($id);
        return view('admin.pekerjaan.edit', compact('data'));
    }

    public function update(PekerjaanRequest $request, $id)
    {
        $data = Pekerjaan::findOrFail($id);
        $data->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Pekerjaan " . $request->pekerjaan;
        History::create($histori);

        $notification = array(
            'message' => 'Data Pekerjaan "' . $request->pekerjaan . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.pekerjaan.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = Pekerjaan::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Pekerjaan " . $data->pekerjaan;
        History::create($histori);

        $notification = array(
            'message' => 'Data Pekerjaan "' . $data->pekerjaan . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.pekerjaan.index')->with($notification);
    }
}
