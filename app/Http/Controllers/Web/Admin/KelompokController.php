<?php

namespace App\Http\Controllers\Web\Admin;

use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\KelompokRequest;
use App\Kelompok;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KelompokController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no = 1;
        $data = Kelompok::orderBy('kelompok', 'ASC')->get()->all();
        return view('admin.kelompok.beranda', compact('data', 'no'));
    }

    public function create()
    {
        //
    }

    public function store(KelompokRequest $request)
    {
        $data = $request->all();
        Kelompok::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Kelompok " . $request->kelompok;
        History::create($histori);

        $notification = array(
            'message' => 'Data Kelompok "' . $request->kelompok . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.kelompok.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Kelompok::findOrFail($id);
        return view('admin.kelompok.edit', compact('data'));
    }

    public function update(KelompokRequest $request, $id)
    {
        $data = Kelompok::findOrFail($id);
        $data->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Kelompok " . $request->kelompok;
        History::create($histori);

        $notification = array(
            'message' => 'Data Kelompok "' . $request->kelompok . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.kelompok.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = Kelompok::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Kelompok " . $data->kelompok;
        History::create($histori);

        $notification = array(
            'message' => 'Data Kelompok "' . $data->kelompok . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.kelompok.index')->with($notification);
    }
}
