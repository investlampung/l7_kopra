<?php

namespace App\Http\Controllers\Web\Admin;

use App\Anggota;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\AnggotaRequest;
use App\Kelompok;
use App\Pekerjaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AnggotaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no = 1;
        $data = Kelompok::orderBy('kelompok', 'ASC')->get()->all();
        return view('admin.anggota.beranda', compact('data', 'no'));
    }

    public function create()
    {
        $kelompok = Kelompok::orderBy('kelompok', 'ASC')->get()->all();
        $pekerjaan = Pekerjaan::orderBy('id', 'ASC')->get()->all();
        return view('admin.anggota.create', compact('kelompok', 'pekerjaan'));
    }

    public function store(AnggotaRequest $request)
    {
        $data = $request->all();
        Anggota::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Anggota " . $request->nama;
        History::create($histori);

        $notification = array(
            'message' => 'Data Anggota "' . $request->nama . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.anggota.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $kelompok = Kelompok::orderBy('kelompok', 'ASC')->get()->all();
        $pekerjaan = Pekerjaan::orderBy('id', 'ASC')->get()->all();
        $data = Anggota::findOrFail($id);
        return view('admin.anggota.edit', compact('data', 'kelompok', 'pekerjaan'));
    }

    public function update(AnggotaRequest $request, $id)
    {
        $data = Anggota::findOrFail($id);
        $data->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Anggota " . $request->nama;
        History::create($histori);

        $notification = array(
            'message' => 'Data Anggota "' . $request->nama . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.anggota.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = Anggota::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Anggota " . $data->nama;
        History::create($histori);

        $notification = array(
            'message' => 'Data Anggota "' . $data->nama . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.anggota.index')->with($notification);
    }
}
