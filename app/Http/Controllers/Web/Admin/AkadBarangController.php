<?php

namespace App\Http\Controllers\Web\Admin;

use App\AkadBarang;
use App\Barang;
use App\History;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AkadBarangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, $id)
    {
        $no = 1;
        $akad_id = $id;
        $data = AkadBarang::where('akad_id', $id)->get()->all();
        return view('admin.akadbarang.beranda', compact('data', 'no', 'akad_id'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $data['akad_id'] = $request->akad_id;
        $data['tanggal'] = $request->tanggal;
        $data['jml'] = $request->jml;

        AkadBarang::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Tanggal Pembayaran Akad.";
        History::create($histori);

        $notification = array(
            'message' => 'Data Tanggal Pembayaran Akad berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = AkadBarang::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Tanggal Pembayaran Akad.";
        History::create($histori);

        $notification = array(
            'message' => 'Data Tanggal Pembayaran Akad berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->back()->with($notification);
    }
}
