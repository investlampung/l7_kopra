<?php

namespace App\Http\Controllers;

use App\Tahun;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $tahun = date("Y");
        $id_tah = Tahun::where('tahun', $tahun)->get()->first();
        $id_tahun = $id_tah->id;

        return redirect()->route('admin.beranda.index')->cookie('tahun', $tahun)->cookie('id_tahun', $id_tahun);
    }
}
