<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BarangRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'barang'                      => 'required',
                        'harga'                      => 'required'
                    ];
                }

            case 'PUT':
            case 'PATCH': {
                    return [
                        'barang'                      => 'required',
                        'harga'                      => 'required'
                    ];
                }

            default:
                break;
        }
    }
    public function messages()
    {
        return [

            'barang.required' => 'Tidak boleh kosong',
            'harga.required' => 'Tidak boleh kosong'
        ];
    }
}
