<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TahunRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'tahun'                      => 'required'
                    ];
                }

            case 'PUT':
            case 'PATCH': {
                    return [
                        'tahun'                      => 'required'
                    ];
                }

            default:
                break;
        }
    }
    public function messages()
    {
        return [

            'tahun.required' => 'Tidak boleh kosong'
        ];
    }
}
