<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AkadRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'no_janji'        => 'required',
                        'no_syariah'      => 'required',
                        'tanggal_janji'   => 'required',
                        'pasal1_a'        => 'required',
                        'pasal1_b'        => 'required',
                        'pasal1_c'        => 'required',
                        'pasal1_e'        => 'required',
                        'pasal1_f'        => 'required',
                        'pasal1_h'        => 'required',
                        'pasal1_i'        => 'required'
                    ];
                }

            case 'PUT':
            case 'PATCH': {
                    return [
                        'no_janji'        => 'required',
                        'no_syariah'      => 'required',
                        'tanggal_janji'   => 'required',
                        'pasal1_a'        => 'required',
                        'pasal1_b'        => 'required',
                        'pasal1_c'        => 'required',
                        'pasal1_e'        => 'required',
                        'pasal1_f'        => 'required',
                        'pasal1_h'        => 'required',
                        'pasal1_i'        => 'required'
                    ];
                }

            default:
                break;
        }
    }
    public function messages()
    {
        return [

            'no_janji.required' => 'Tidak boleh kosong',
            'no_syariah.required' => 'Tidak boleh kosong',
            'tanggal_janji.required' => 'Tidak boleh kosong',
            'pasal1_a.required' => 'Tidak boleh kosong',
            'pasal1_b.required' => 'Tidak boleh kosong',
            'pasal1_c.required' => 'Tidak boleh kosong',
            'pasal1_e.required' => 'Tidak boleh kosong',
            'pasal1_f.required' => 'Tidak boleh kosong',
            'pasal1_h.required' => 'Tidak boleh kosong',
            'pasal1_i.required' => 'Tidak boleh kosong'
        ];
    }
}
