<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KelompokRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'kelompok'                      => 'required'
                    ];
                }

            case 'PUT':
            case 'PATCH': {
                    return [
                        'kelompok'                      => 'required'
                    ];
                }

            default:
                break;
        }
    }
    public function messages()
    {
        return [

            'kelompok.required' => 'Tidak boleh kosong',
            'jml_setoran.required' => 'Tidak boleh kosong'
        ];
    }
}
