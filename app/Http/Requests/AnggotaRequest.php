<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnggotaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'nama'                      => 'required',
                        'no_ktp'                    => 'required',
                        'kode_rekening'             => 'required',
                        'no_telp'                   => 'required'
                    ];
                }

            case 'PUT':
            case 'PATCH': {
                    return [
                        'nama'                      => 'required',
                        'no_ktp'                    => 'required',
                        'kode_rekening'             => 'required',
                        'no_telp'                   => 'required'
                    ];
                }

            default:
                break;
        }
    }
    public function messages()
    {
        return [

            'nama.required' => 'Tidak boleh kosong',
            'no_ktp.required' => 'Tidak boleh kosong',
            'kode_rekening.required' => 'Tidak boleh kosong',
            'no_telp.required' => 'Tidak boleh kosong'
        ];
    }
}
