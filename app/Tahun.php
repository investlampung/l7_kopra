<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tahun extends Model
{
    protected $guarded = [];
    public function bulan()
    {
        return $this->hasMany('App\Bulan');
    }
    public function bangun()
    {
        return $this->hasMany('App\BangunRumah');
    }
}
