<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BangunRumah extends Model
{
    protected $guarded = [];
    public function anggota()
    {
        return $this->belongsTo('App\Anggota', 'anggota_id');
    }
    public function tahun()
    {
        return $this->belongsTo('App\Tahun', 'tahun_id');
    }
}
