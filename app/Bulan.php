<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bulan extends Model
{
    protected $guarded = [];
    public function tahun()
    {
        return $this->belongsTo('App\Tahun', 'tahun_id');
    }

    public function angsuran()
    {
        return $this->hasMany('App\Angsuran');
    }
}
