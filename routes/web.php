<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);

//ADMIN
//Beranda
Route::get('home', 'HomeController@index');
Route::resource('admin/beranda', 'Web\Admin\BerandaController', ['as' => 'admin']);
Route::resource('admin/pekerjaan', 'Web\Admin\PekerjaanController', ['as' => 'admin']);
Route::resource('admin/tahun', 'Web\Admin\TahunController', ['as' => 'admin']);
Route::resource('admin/bulan', 'Web\Admin\BulanController', ['as' => 'admin']);
Route::resource('admin/kelompok', 'Web\Admin\KelompokController', ['as' => 'admin']);
Route::resource('admin/anggota', 'Web\Admin\AnggotaController', ['as' => 'admin']);
Route::resource('admin/angsuran', 'Web\Admin\AngsuranController', ['as' => 'admin']);
Route::resource('admin/triwulan', 'Web\Admin\TriwulanController', ['as' => 'admin']);
Route::resource('admin/tahfidz', 'Web\Admin\TahfidzController', ['as' => 'admin']);
Route::resource('admin/rubah', 'Web\Admin\RubahController', ['as' => 'admin']);

Route::resource('admin/akad', 'Web\Admin\AkadController', ['as' => 'admin']);
Route::get('admin-akad-ajax', 'Web\Admin\AkadController@dataAjax');

Route::resource('admin/barang', 'Web\Admin\BarangController', ['as' => 'admin']);
Route::resource('admin/akadbarang', 'Web\Admin\AkadBarangController', ['as' => 'admin']);
Route::resource('admin/bangunrumah', 'Web\Admin\BangunRumahController', ['as' => 'admin']);
Route::get('admin/akadbarang/index/{id}', 'Web\Admin\AkadBarangController@index', ['as' => 'admin']);
Route::resource('admin/history', 'Web\Admin\HistoryController', ['as' => 'admin']);

Route::get('admin/upload/anggota', 'Web\Admin\UploadController@anggota')->name('admin.upload.anggota');
Route::post('admin/import/anggota', 'Web\Admin\UploadController@import_anggota')->name('admin.import.anggota');

Route::get('admin/upload/akad', 'Web\Admin\UploadController@akad')->name('admin.upload.akad');
Route::post('admin/import/akad', 'Web\Admin\UploadController@import_akad')->name('admin.import.akad');

Route::get('admin/upload/jpa', 'Web\Admin\UploadController@jpa')->name('admin.upload.jpa');
Route::post('admin/import/jpa', 'Web\Admin\UploadController@import_jpa')->name('admin.import.jpa');

Route::get('admin/upload/regsim', 'Web\Admin\UploadController@regsim')->name('admin.upload.regsim');
Route::post('admin/import/regsim', 'Web\Admin\UploadController@import_regsim')->name('admin.import.regsim');

Route::get('admin/upload/regpem', 'Web\Admin\UploadController@regpem')->name('admin.upload.regpem');
Route::post('admin/import/regpem', 'Web\Admin\UploadController@import_regpem')->name('admin.import.regpem');

Route::get('admin/upload/bangunrumah', 'Web\Admin\UploadController@bangunrumah')->name('admin.upload.bangunrumah');
Route::post('admin/import/bangunrumah', 'Web\Admin\UploadController@import_bangunrumah')->name('admin.import.bangunrumah');

Route::get('admin/triwulan/show/{id}/{tw}', 'Web\Admin\TriwulanController@show_triwulan')->name('admin.triwulan.show.tw');
Route::get('admin/tahfidz/show/{id}/{tw}', 'Web\Admin\TahfidzController@show_tahfidz')->name('admin.tahfidz.show.tw');
