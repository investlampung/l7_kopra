<!DOCTYPE html>
<html lang="id">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="shortcut icon" href="{{asset('itlabil/images/default/logo.jpg')}}">
  <title>Admin Arisan Rumah</title>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- CSS -->

  <link href="{{ asset('itlabil/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/bower_components/Ionicons/css/ionicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/dist/css/AdminLTE.min.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/bower_components/morris.js/morris.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/bower_components/jvectormap/jquery-jvectormap.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link rel="stylesheet" href="{{ asset('itlabil/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <link href="{{ asset('itlabil/admin/toast/toastr.min.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/dist/css/editsendiri.css') }}" rel="stylesheet">

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- HEADER -->
    <header class="main-header">
      <a href="{{ asset('admin/beranda') }}" class="logo">
        <span class="logo-mini">AR</span>
        <span class="logo-lg"><b>Arisan Rumah</b></span>
      </a>
      <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">

            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="{{ asset('itlabil/admin/dist/img/avatar5.png') }}" class="user-image" alt="User Image">
                <span class="hidden-xs">{{Auth::user()->name}}</span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="{{ asset('itlabil/admin/dist/img/avatar5.png') }}" class="img-circle" alt="User Image">
                  <p>{{Auth::user()->name}}</p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div align="center">
                    <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                    </form>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <!-- MENU SIDE BAR -->
    <aside class="main-sidebar">
      <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
          <li style="background-color:#206288;"><a href="#" data-toggle="modal" data-target="#modal-tahun"><span>Tahun Aktif : {{ Cookie::get('tahun') }}</span></a></li>
          <li><a href="{{ asset('admin/beranda') }}"><i class="fa fa-home"></i> <span>Beranda</span></a></li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-database"></i>
              <span>Data Master</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{ asset('admin/tahun') }}"><i class="fa fa-circle-o"></i> Tahun</a></li>
              <li><a href="{{ asset('admin/pekerjaan') }}"><i class="fa fa-circle-o"></i> Pekerjaan</a></li>
              <li><a href="{{ asset('admin/kelompok') }}"><i class="fa fa-circle-o"></i> Kelompok</a></li>
              <li><a href="{{ asset('admin/bulan') }}"><i class="fa fa-circle-o"></i> Bulan</a></li>
              <li><a href="{{ asset('admin/bangunrumah') }}"><i class="fa fa-circle-o"></i> Bangun Rumah</a></li>
            </ul>
          </li>
          <li><a href="{{ asset('admin/anggota') }}"><i class="fa fa-group"></i> <span>Anggota</span></a></li>
          <li><a href="{{ asset('admin/angsuran') }}"><i class="fa fa-money"></i> <span>Angsuran</span></a></li>
          <li><a href="{{ asset('admin/triwulan') }}"><i class="fa fa-file"></i> <span>Triwulan</span></a></li>
          <li><a href="{{ asset('admin/tahfidz') }}"><i class="fa fa-file"></i> <span>Tahfidz</span></a></li>
          <li><a href="{{ asset('admin/akad') }}"><i class="fa fa-handshake-o"></i> <span>Akad</span></a></li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-arrow-up"></i>
              <span>Upload Data</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{ asset('admin/upload/anggota') }}"><i class="fa fa-circle-o"></i> Anggota</a></li>
              <li><a href="{{ asset('admin/upload/akad') }}"><i class="fa fa-circle-o"></i> Akad</a></li>
              <li><a href="{{ asset('admin/upload/bangunrumah') }}"><i class="fa fa-circle-o"></i> Bangun Rumah</a></li>
              <li><a href="{{ asset('admin/upload/jpa') }}"><i class="fa fa-circle-o"></i> Pembayaran Angsuran</a></li>
              <li><a href="{{ asset('admin/upload/regsim') }}"><i class="fa fa-circle-o"></i> Registrasi Simpanan</a></li>
              <li><a href="{{ asset('admin/upload/regpem') }}"><i class="fa fa-circle-o"></i> Registrasi Pembiayaan</a></li>
            </ul>
          </li>
          <li><a href="{{ asset('admin/history') }}"><i class="fa fa-list"></i> <span>History</span></a></li>
        </ul>
      </section>
    </aside>
    <!-- TAHUN -->
    <div class="modal fade in" id="modal-tahun">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Tahun Aktif</h4>
          </div>
          <form action="{{ route('admin.rubah.store') }}" class="form-horizontal" method="POST">
            @csrf

            <div class="box-body">
              <div class="form-group">
                <label for="tahun" class="col-sm-2 control-label">Tahun</label>

                <div class="col-sm-10">
                  <select class="form-control" name="tahun">
                    @foreach(App\Tahun::select('tahun')->get()->all() as $all_tahun)
                    <option value="{{$all_tahun->tahun}}">{{$all_tahun->tahun}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <button class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
              <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            </div>

          </form>
        </div>
      </div>
    </div>

    <!-- CONTENT -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        @yield('content')
      </section>
    </div>

    <!-- FOOTER -->
    <footer class="main-footer">
      <strong>Copyright &copy; 2020 Koperasi Arisan Rumah
    </footer>

    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- JS -->
  <script src="{{ asset('itlabil/admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <script src="{{ asset('itlabil/admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/raphael/raphael.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/morris.js/morris.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/moment/min/moment.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/fastclick/lib/fastclick.js') }}"></script>
  <script src="{{ asset('itlabil/admin/dist/js/adminlte.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/dist/js/pages/dashboard.js') }}"></script>
  <script src="{{ asset('itlabil/admin/dist/js/demo.js') }}"></script>


  <!-- DataTables -->
  <script src="{{ asset('itlabil/admin/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

  <script type="text/javascript" src="{{ asset('itlabil/admin/toast/toastr.min.js') }}"></script>

  <script>
    @if(Session::has('message'))
    var type = "{{Session::get('alert-type','info')}}"

    switch (type) {
      case 'info':
        toastr.info("{{ Session::get('message') }}");
        break;
      case 'success':
        toastr.success("{{ Session::get('message') }}");
        break;
      case 'warning':
        toastr.warning("{{ Session::get('message') }}");
        break;
      case 'error':
        toastr.error("{{ Session::get('message') }}");
        break;
    }
    @endif
  </script>

  <script>
    $(function() {
      $('#example1').DataTable()
      $('#example2').DataTable()
      $('#example3').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
      })
    })
  </script>

  <script>
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#category-img-tag').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#cat_image").change(function() {
      readURL(this);
    });
  </script>
</body>

</html>