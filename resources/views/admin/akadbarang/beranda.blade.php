@extends('layouts.admin')

@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Jadwal Pembayaran Angsuran
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Jadwal Pembayaran Angsuran</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.akadbarang.store') }}" class="form-horizontal" method="POST">
                        @csrf

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tanggal Pembayaran</label>

                                <div class="col-sm-8">
                                    <input type="date" name="tanggal" class="form-control">
                                    <small class="text-danger">{{ $errors->first('tanggal') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Jumlah</label>

                                <div class="col-sm-8">
                                    <input type="number" name="jml" class="form-control" placeholder="Jumlah">
                                    <small class="text-danger">{{ $errors->first('barang') }}</small>
                                </div>
                            </div>
                            <input type="hidden" name="akad_id" value="{{$akad_id}}">
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
                <hr>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Jumlah</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ tanggal_local($item->tanggal) }}</td>
                                <td>Rp. {{ number_format($item->jml, 0, ".", ".")}},-</td>
                                <td align="center">
                                    <form action="{{ route('admin.akadbarang.destroy',$item->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <a href="{{url('admin/akad')}}" class="btn btn-default">Kembali</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection