@extends('layouts.admin')

@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Angsuran
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Angsuran</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.angsuran.store') }}" class="form-horizontal" method="POST">
                        @csrf

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Anggota</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="anggota_id">
                                        @foreach($data as $item)
                                        @foreach($item->anggota->sortBy('nama') as $anggota)
                                        <option value="{{$anggota->id}}">{{$item->kelompok}} | {{$anggota->nama}}</option>
                                        @endforeach
                                        @endforeach
                                    </select>
                                    <small class="text-danger">{{ $errors->first('anggota_id') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Bulan</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="bulan_id">
                                        @foreach($bulan as $bul)
                                        <option value="{{$bul->id}}">{{$bul->bulan}}</option>
                                        @endforeach
                                    </select>
                                    <small class="text-danger">{{ $errors->first('bulan_id') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Angsuran</label>

                                <div class="col-sm-8">
                                    <input type="number" name="dana" class="form-control" placeholder="Angsuran">
                                    <small class="text-danger">{{ $errors->first('dana') }}</small>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
                <hr>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kelompok</th>
                                <th>Anggota</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($data as $item)
                            @foreach($item->anggota->sortBy('nama') as $anggota)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->kelompok }}</td>
                                <td>{{ $anggota->nama }}</td>
                                <td align="center">
                                    <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#modal-barang{{$anggota->id }}">Lihat</a>
                                </td>
                            </tr>
                            <!-- Angsuran -->
                            <div class="modal fade in" id="modal-barang{{$anggota->id}}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                            <h4 class="modal-title">
                                                Data Angsuran - {{$anggota->nama}}
                                            </h4>
                                        </div>
                                        <div class="box-body">
                                            @php
                                            $nop=1;
                                            @endphp
                                            <div class="col-md-1">
                                                No
                                            </div>
                                            <div class="col-md-4">
                                                Bulan
                                            </div>
                                            <div class="col-md-4">
                                                Angsuran
                                            </div>
                                            <div class="col-md-3">
                                                Aksi
                                            </div>
                                        </div>
                                        @foreach($anggota->angsuran->sortBy('bulan_id') as $angsuran)
                                        <div class="box-body">
                                            <div class="col-md-1">
                                                {{$nop++}}
                                            </div>
                                            <div class="col-md-4">
                                                {{$angsuran->bulan->bulan}}
                                            </div>
                                            <div class="col-md-4">
                                                Rp. {{ number_format($angsuran->dana, 0, ".", ".")}},-
                                            </div>
                                            <div class="col-md-3">
                                                <form action="{{ route('admin.angsuran.destroy',$angsuran->id) }}" method="POST">
                                                    <a class="btn btn-success" href="{{ route('admin.angsuran.edit',$angsuran->id) }}"><span><i class="fa fa-edit"></i></span></a>
                                                    @csrf
                                                    @method('DELETE')

                                                    <button type="submit" class="btn btn-danger"><span><i class="fa fa-trash"></i></span></button>
                                                </form>
                                            </div>
                                        </div>
                                        @endforeach
                                        <div class="box-footer">
                                            <button class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection