@extends('layouts.admin')
@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Angsuran
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Ubah Data Angsuran</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.angsuran.update', $data->id) }}" class="form-horizontal" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Anggota</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="anggota_id">
                                        <option value="{{$data->anggota_id}}">{{$data->anggota->kelompok->kelompok}} | {{$data->anggota->nama}}</option>
                                        @foreach($kelompok as $item)
                                        @foreach($item->anggota->sortBy('nama') as $anggota)
                                        <option value="{{$anggota->id}}">{{$item->kelompok}} | {{$anggota->nama}}</option>
                                        @endforeach
                                        @endforeach
                                    </select>
                                    <small class="text-danger">{{ $errors->first('anggota_id') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Bulan</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="bulan_id">
                                        <option value="{{$data->bulan_id}}">{{$data->bulan->bulan}}</option>
                                        @foreach($bulan as $bul)
                                        <option value="{{$bul->id}}">{{$bul->bulan}}</option>
                                        @endforeach
                                    </select>
                                    <small class="text-danger">{{ $errors->first('bulan_id') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Angsuran</label>

                                <div class="col-sm-8">
                                    <input type="number" value="{{$data->dana}}" name="dana" class="form-control" placeholder="Angsuran">
                                    <small class="text-danger">{{ $errors->first('dana') }}</small>
                                </div>
                            </div>

                            <a href="{{url('admin/angsuran')}}" class="btn btn-default">Kembali</a>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection