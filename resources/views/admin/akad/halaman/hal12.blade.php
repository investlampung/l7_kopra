<table width="100%">
    <tbody>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Apabila dikemudian hari terjadi perselisihan dalam penafsiran atau pelaksanaan ketentuan- ketentuan dari Akad ini, maka para pihak sepakat untuk terlebih dahulu menyelesaikan secara musyawarah.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">3.</td>
            <td colspan="2" class="kanankiri">
                Bilamana musyawarah tidak menghasilkan kata sepakat mengenai penyelesaian perselisihan, maka semua sengketa yang timbul dari Akad ini akan diselesaikan dan diputus oleh Pengadialan Agama yang keputusannya mengikat kedua belah pihak yang bersengketa, sebagai keputusan tingkat pertama dan terakhir.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">4.</td>
            <td colspan="2" class="kanankiri">
                Mengenai pelaksanaan (eksekusi) putusan Pengadilan Agama, sesuai dengan ketentuan Undang- Undang tentang Arbitrase dan Alternatif Penyelesaian Sengketa, PARA PIHAK sepakat bahwa <b>PENYELENGGARA</b> dapat meminta pelaksanaan (eksekusi) putusan Pengadilan Agama tersebut pada setiap Pengadilan Negeri di wilayah hukum Republik Indonesia.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 23<br>
                    LAIN - LAIN<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                Dalam hal <b>PENERIMA PEMBIAYAAN</b> menyampaikan pernyataan yang tidak benar mengenai financing to value ratio maka <b>PENERIMA PEMBIAYAAN</b> bersedia melaksanakan langkah-langkah yang ditetapkan oleh <b>PENYELENGGARA</b> dalam rangka pemenuhan ketentuan <b>PENYELENGGARA</b> Indonesia atau institusi yang berwenang.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Semua pemberitahuan tertulis dari <b>PENYELENGGARA</b> dan semua surat menyurat antara <b>PENYELENGGARA</b> dan <b>PENERIMA PEMBIAYAAN</b> dalam pelaksanaan Akad ini mengikat dan harus ditaati oleh <b>PENERIMA PEMBIAYAAN</b> .
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 24<br>
                    PENUTUP<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                Uraian pasal demi pasal Akad ini, telah dibaca, dimengerti dan dipahami serta disetujui oleh <b>PENERIMA PEMBIAYAAN</b> dan <b>PENYELENGGARA</b>.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Segala sesuatu yang belum diatur atau perubahan dalam Akad ini akan di atur dalam surat- menyurat berdasarkan kesepakatan bersama antara <b>PENYELENGGARA</b> dan <b>PENERIMA PEMBIAYAAN</b> yang merupakan bagian yang tidak terpisahkan dari Akad ini.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">3.</td>
            <td colspan="2" class="kanankiri">
                Akad ini mulai berlaku sejak tanggal ditandatanganinya.
            </td>
        </tr>
    </tbody>
</table>
<table width="100%">
    <tbody>
        <tr>
            <td width="30%"></td>
            <td></td>
            <td align="center" width="40%"><br>GADINGREJO,
                @if(empty($data->pasal24_a))
                ........
                @else
                {{ tanggal_local($data->pasal24_a) }}
                @endif
                <br><br></td>
        </tr>
        <tr>
            <td valign="top" align="center"><b><b>PENERIMA PEMBIAYAAN</b></b></td>
            <td></td>
            <td align="center"><b><b>PENYELENGGARA</b><br>KOPERASI MITRA GRIYA INDONESIA</b><br><br><br><br><br></td>
        </tr>
        <tr>
            <td align="center"><b>{{$data->anggota->nama}}</b></td>
            <td></td>
            <td align="center"><b>Wahyudi</b><br><br></td>
        </tr>
    </tbody>
</table>