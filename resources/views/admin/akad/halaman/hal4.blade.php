<table width="100%">
    <tbody>
        <tr>
            <td width="20px" valign="top">3.</td>
            <td colspan="2" class="kanankiri">
                <b>PENERIMA PEMBIAYAAN</b> bersedia membayar Harga Jual Barang sesuai Akad ini, dan Harga Jual tidak dapat berubah selama berlakunya Akad ini.
            </td>
        </tr>
        <tr>
            <td width="20px" valign="top">4.</td>
            <td colspan="2" class="kanankiri">
                <b>PENYELENGGARA</b> dengan Akad ini mewakilkan secara penuh kepada <b>PENERIMA PEMBIAYAAN</b> untuk membeli dan menerima Barang dari Pemasok, serta memberi hak melakukan pembuatan akta jual beli untuk dan atas nama <b>PENERIMA PEMBIAYAAN</b> sendiri langsung dengan Pemasok.
            </td>
        </tr>
        <tr>
            <td width="20px" valign="top">5.</td>
            <td colspan="2" class="kanankiri">
                Pemberian kuasa sebagaimana dimaksud dalam ayat 4 pasal ini, tidak mengakibatkan <b>PENERIMA PEMBIAYAAN</b> dapat membatalkan jual beli Barang serta <b>PENERIMA PEMBIAYAAN</b> tidak dapat menuntut <b>PENYELENGGARA</b> untuk memberikan ganti rugi sebagaimana dimaksud dalam pasal 1471 Kitab Undang-Undang Hukum Perdata.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 4<br>
                    SYARAT REALISASI PEMBIAYAAN<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td width="20px" valign="top">1.</td>
            <td colspan="2" class="kanankiri">
                <b>PENYELENGGARA</b> akan merealisasikan Pembiayaan berdasarkan Akad ini, setelah <b>PENERIMA PEMBIAYAAN</b> terlebih dahulu memenuhi seluruh persyaratan sebagai berikut:
            </td>
        </tr>
        <tr>
            <td width="20px" valign="top"></td>
            <td width="20px" valign="top">a.</td>
            <td class="kanankiri">
                Menyerahkan kepada <b>PENYELENGGARA</b> seluruh dokumen yang disyaratkan oleh <b>PENYELENGGARA</b> termasuk tetapi tidak terbatas pada dokumen bukti diri <b>PENERIMA PEMBIAYAAN</b> , dokumen kepemilikan jaminan dan atau surat lainnya yang berkaitan dengan Akad ini dan pengikatan jaminan, yang ditentukan dalam Surat Penawaran Pembiayaan dari <b>PENYELENGGARA</b>.
            </td>
        </tr>
        <tr>
            <td width="20px" valign="top"></td>
            <td width="20px" valign="top">b.</td>
            <td class="kanankiri">
                <b>PENERIMA PEMBIAYAAN</b> wajib membuka dan memelihara akun pada <b>PENYELENGGARA</b> selama <b>PENERIMA PEMBIAYAAN</b> mempunyai Pembiayaan Murabahah dari <b>PENYELENGGARA</b>.
            </td>
        </tr>
        <tr>
            <td width="20px" valign="top"></td>
            <td width="20px" valign="top">c.</td>
            <td class="kanankiri">
                Menandatangani Akad ini dan perjanjian pengikatan jaminan yang disyaratkan oleh <b>PENYELENGGARA</b>.
            </td>
        </tr>
        <tr>
            <td width="20px" valign="top"></td>
            <td width="20px" valign="top">d.</td>
            <td class="kanankiri">
                Menyetorkan uang muka pembelian dan atau biaya-biaya yang disyaratkan oleh <b>PENYELENGGARA</b> sebagai yang tercantum dalam Surat Penawaran Pembiayaan.
            </td>
        </tr>
        <tr>
            <td width="20px" valign="top">2.</td>
            <td colspan="2" class="kanankiri">
                Realisasi Pembiayaan akan dilakukan oleh <b>PENYELENGGARA</b> kepada Pemasok, baik secara langsung maupun melalui <b>PENERIMA PEMBIAYAAN</b> .
            </td>
        </tr>
        <tr>
            <td width="20px" valign="top">3.</td>
            <td colspan="2" class="kanankiri">
                Sejak ditandatanganinya Akad ini dan telah diterimanya Barang pesanan oleh <b>PENERIMA PEMBIAYAAN</b> , maka risiko atas Barang tersebut sepenuhnya menjadi tanggung jawab <b>PENERIMA PEMBIAYAAN</b> dan dengan ini <b>PENERIMA PEMBIAYAAN</b> membebaskan <b>PENYELENGGARA</b> dari segala tuntutan dan atau ganti rugi berupa apapun atas risiko tersebut.
            </td>
        </tr>
        <tr>
            <td width="20px" valign="top">4.</td>
            <td colspan="2" class="kanankiri">
                Apabila <b>PENYELENGGARA</b> telah membayar kepada Pemasok termasuk pembayaran uang muka, maka <b>PENERIMA PEMBIAYAAN</b> tidak dapat membatalkan secara sepihak Akad ini.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 5<br>
                    JATUH TEMPO PEMBIAYAAN<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="kanankiri">
                Fasilitas pembiayaan yang dimaksud dalam Akad ini berlangsung untuk jangka waktu <b>{{$data->pasal5_a}}</b> bulan terhitung sejak tanggal Akad ini ditandatangani serta berakhir pada tanggal {{$data->pasal5_b}} ( {{terbilang($data->pasal5_b)}} )
            </td>
        </tr>
    </tbody>
</table>