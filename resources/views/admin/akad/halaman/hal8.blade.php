<table width="100%">
    <tbody>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="3" class="kanankiri">
                Apabila <b>PENERIMA PEMBIAYAAN</b> melakukan wanprestasi, maka <b>PENYELENGGARA</b> berhak setiap saat melakukan tindakan terhadap barang yang dijaminkan yaitu:
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">a.</td>
            <td colspan="3" class="kanankiri">
                Memasuki pekarangan, barang berikut tanah yang menjadi jaminan dan atau memasuki pekarangan, barang berikut tanah dimana barang jaminan tersebut disimpan.</td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">b.</td>
            <td colspan="3" class="kanankiri">
                Melakukan pemeriksaan atas keadaan barang berikut fasilitasnya yang melekat serta mendapatkan keterangan secara langsung ataupun tidak langsung dari <b>PENERIMA PEMBIAYAAN</b> dan atau dari siapa pun mengenai hal-hal yang perlu diketahui oleh <b>PENYELENGGARA</b>.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">c.</td>
            <td colspan="3" class="kanankiri">
                Melakukan tindakan-tindakan sebagaimana dimaksud dalam Pasal 13 ayat 2.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="4">
                <b>
                    <br>PASAL 15<br>
                    TANGGUNG JAWAB PARA-PIHAK<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="3" class="kanankiri">
                Pilihan atas Barang yang akan dibeli dengan Pembiayaan <b>PENYELENGGARA</b>, sepenuhnya menjadi tanggung jawab <b>PENERIMA PEMBIAYAAN</b> sebagai pembeli.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="3" class="kanankiri">
                Apabila kemudian hari diketahui atau timbul cacat, kekurangan atau keadaan/masalah apapun yang menyangkut Barang dan atau pelaksanaan Akad/Akta Jual Beli barang dan tanah, jual beli mana seluruh atau sebagian dibiayai dengan Pembiayaan <b>PENYELENGGARA</b>, maka segala risiko sepenunya menjadi tanggung jawab <b>PENERIMA PEMBIAYAAN</b> .
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">3.</td>
            <td colspan="3" class="kanankiri">
                Adanya cacat kekurangan atau masalah yang timbul tidak dapat dijadikan alasan untuk mengingkari, melalaikan atau menunda pelaksanaan kewajiban <b>PENERIMA PEMBIAYAAN</b> kepada <b>PENYELENGGARA</b> sesuai Akad ini, termasuk antara lain membayar angsuran dan sebagainya.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">4.</td>
            <td colspan="3" class="kanankiri">
                <b>PENYELENGGARA</b> tidak bertanggung jawab terhadap penyelesaian surat/dokumen atas Barang yang dibeli dengan Pembiayaan Murabahah yang menjadi tanggung jawab Pemasok.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="4">
                <b>
                    <br>PASAL 16<br>
                    PENAGIHAN SEKETIKA SELURUH HUTANG MURABAHAN DAN<br>
                    PENYERAHAN / PENGOSONGAN BARANG<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="3" class="kanankiri">
                Menyimpang dari jangka waktu Pembiayaan, <b>PENYELENGGARA</b> berhak mengakhiri jangka waktu Pembiayaan dan menagih pelunasan sekaligus atas seluruh sisa Hutang dan <b>PENERIMA PEMBIAYAAN</b> wajib membayar dengan seketika dan sekaligus melunasi sisa Hutang yang ditagih oleh <b>PENYELENGGARA</b> atau melakukan upaya-upaya hukum lain untuk menyelesaikan Pembiayaan, bila <b>PENERIMA PEMBIAYAAN</b> ternyata tidak memenuhi kewajibannya yaitu:
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">a.</td>
            <td colspan="3" class="kanankiri">
                <b>PENERIMA PEMBIAYAAN</b> wanprestasi.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">b.</td>
            <td colspan="3" class="kanankiri">
                <b>PENERIMA PEMBIAYAAN</b> diperkirakan tidak akan mampu lagi untuk memenuhi sesuatu ketentuan atau kewajiban di dalam Akad ini, karena terjadinya antara lain peristiwa sebagai berikut:
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">(1)</td>
            <td colspan="2" class="kanankiri">
                <b>PENERIMA PEMBIAYAAN</b> diberhentikan dari Kantor/Instansi yang bersangkutan, dijatuhi hukuman Pidana, mendapat cacat badan, sehingga oleh karenanya belum/tidak dapat dipekerjakan lagi atau
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">(2)</td>
            <td colspan="2" class="kanankiri">
                <b>PENERIMA PEMBIAYAAN</b> telah dinyatakan pailit atau tidak mampu membayar atau telah dikeluarkan perintah oleh pejabat yang berwenang untuk menunjuk wakil atau kuratornya.
            </td>
        </tr>
    </tbody>
</table>