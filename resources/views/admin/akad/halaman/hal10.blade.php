<table width="100%">
    <tbody>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Apabila <b>PENERIMA PEMBIAYAAN</b> karena tidak mampu lagi memenuhi kewajibannya untuk membayar angsuran guna melunasi kembali Pembiayaan dan atas dasar itu <b>PENERIMA PEMBIAYAAN</b> menyerahkan barang yang dijadikan jaminan Pembiayaan kepada <b>PENYELENGGARA</b>A, <b>PENYELENGGARA</b>A berhak melaksanakan tindakan-tindakan tersebut pada ayat 1 pasal ini.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">3.</td>
            <td colspan="2" class="kanankiri">
                Apabila, <b>PENYELENGGARA</b>A menggunakan haknya untuk menagih pelunasan sekaligus atas Hutang <b>PENERIMA PEMBIAYAAN</b> dan <b>PENERIMA PEMBIAYAAN</b> tidak dapat memenuhi kewajibannya membayar pelunasan tersebut, <b>PENYELENGGARA</b>A berhak untuk setiap saat melaksanakan hak eksekusinya atas penjualan Barang jaminan yang dipegangnya menurut cara dan harga yang dianggap baik oleh <b>PENYELENGGARA</b>A termasuk dan tidak terkecuali <b>PENYELENGGARA</b>A berhak sepenuhnya mencarikan <b>PENERIMA PEMBIAYAAN</b> baru untuk mengambil alih atau mengoper Hutang <b>PENERIMA PEMBIAYAAN</b> , dan dengan Akad ini <b>PENERIMA PEMBIAYAAN</b> memberikan kuasa kepada <b>PENYELENGGARA</b>A untuk melakukan segala tindakan guna melaksanakan maksud tersebut diatas, tanpa ada tindakan yang dikecualikan.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">4.</td>
            <td colspan="2" class="kanankiri">
                Hasil eksekusi dan atau penjualan barang jaminan tersebut diprioritaskan untuk melunasi seluruh sisa Hutang <b>PENERIMA PEMBIAYAAN</b> kepada <b>PENYELENGGARA</b>A, termasuk semua biaya yang telah dikeluarkan <b>PENYELENGGARA</b>A guna melaksanakan penjualan atau eksekusi Barang jaminan, dan apabila masih ada sisanya maka jumlah sisa tersebut akan dibayarkan kepada <b>PENERIMA PEMBIAYAAN</b> .
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 18<br>
                    PENGALIHAN PIUTANG MURABAHAN KEPADA PIHAK LAIN<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                <b>PENERIMA PEMBIAYAAN</b> menyetujui dan sepakat untuk memberikan hak sepenuhnya kepada <b>PENYELENGGARA</b>A untuk mengalihkan piutang Murabahah (cessie) dan atau tagihan <b>PENYELENGGARA</b>A terhadap <b>PENERIMA PEMBIAYAAN</b> berikut semua janji-janji accessoirnya, termasuk hak-hak jaminan atas Pembiayaan kepada pihak lain yang ditetapkan oleh <b>PENYELENGGARA</b>A sendiri, setiap saat diperlukan oleh <b>PENYELENGGARA</b>A dan dengan Akad ini <b>PENERIMA PEMBIAYAAN</b> memberikan kuasa kepada <b>PENYELENGGARA</b>A dan <b>PENYELENGGARA</b>A berhak untuk melakukan segala tindakan guna melaksanakan maksud tersebut diatas, tanpa ada tindakan yang dikecualikan.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Apabila <b>PENYELENGGARA</b>A melaksanakan penyerahan piutang Murabahah (cessie) kepada pihak lain sebagaimana dimaksud pada ayat 1 pasal ini dan pengelolaan Pembiayaan tetap dilakukan oleh <b>PENYELENGGARA</b>A, maka <b>PENYELENGGARA</b>A tidak wajib memberitahukan kepada <b>PENERIMA PEMBIAYAAN</b> , sehingga apabila kemudian pihak yang menerima penyerahan piutang Murabahah (menerima cessie) menjalankan haknya sebagai penerima pengalihan piutang, maka hal demikian sudah dapat dinyatakan sepenuhnya semata-mata berdasarkan Akad ini yang dibuat antara <b>PENYELENGGARA</b>A dengan pihak yang menerima penyerahan piutang Murabahah dan adanya pengalihan piutang Murabahah ini tidak mempengaruhi sama sekali pelaksanaan kewajiban <b>PENERIMA PEMBIAYAAN</b> sesuai dengan Akad ini. Apabila pengelolaan Pembiayaan tidak dilakukan oleh <b>PENYELENGGARA</b>A setelah piutang dialihkan, maka <b>PENYELENGGARA</b>A wajib memberitahukan adanya pengalihan piutang tersebut kepada <b>PENERIMA PEMBIAYAAN</b> .
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 19<br>
                    TIMBUL DAN BERAKHIRNYA HAK - HAK DAN KEWAJIBAN<br><br>
                </b>
            </td>
        </tr>
    </tbody>
</table>