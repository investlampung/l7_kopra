<table width="100%">
    <tbody>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                Dalam hal seluruh Hutang telah dilunasi, <b>PENYELENGGARA</b> wajib menyerahkan kembali semua surat-surat dan atau dokumen-dokumen mengenai barang jaminan, serta surat-surat bukti lainnya yang disimpan atau dikuasai <b>PENYELENGGARA</b> kepada:
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">a.</td>
            <td colspan="2" class="kanankiri">
                <b>PENERIMA PEMBIAYAAN</b>.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">b.</td>
            <td colspan="2" class="kanankiri">
                Pemenang lelang eksekusi jaminan.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">c.</td>
            <td colspan="2" class="kanankiri">
                Pihak lain berdasarkan Penetapan atau Putusan Pengadilan yang berkekuatan hukum tetap; atau
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">d.</td>
            <td colspan="2" class="kanankiri">
                Ahli Waris <b>PENERIMA PEMBIAYAAN</b>.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Bila <b>PENERIMA PEMBIAYAAN</b> meninggal dunia, hak dan kewajibannya beralih kepada ahli waris dan <b>PENYELENGGARA</b> berhak untuk meminta kepada ahli warisnya turunan akta kematian yang dilegalisir oleh pejabat atau instansi yang berwenang disamping surat keterangan hak waris, akta wasiat atau bukti-bukti lainnya, yang menurut pertimbangan <b>PENYELENGGARA</b> diperlukan untuk mengetahui ahli waris yang sah.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 20<br>
                    KUASA YANG TIDAK DAPAT DITARIK KEMBALI<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="kanankiri">
                Semua kuasa yang dibuat dan diberikan oleh <b>PENERIMA PEMBIAYAAN</b> dalam rangka Akad ini merupakan satu kesatuan yang tak terpisahkan dari Akad ini dan tidak dapat ditarik kembali karena sebab-sebab apapun juga yang dapat mengakhiri kuasa terutama yang dimaksud dalam Pasal 1813 Kitab Undang-Undang Hukum Perdata sampai dengan Pembiayaan lunas, dan <b>PENERIMA PEMBIAYAAN</b> mengikatkan serta mewajibkan diri untuk tidak membuat surat-surat kuasa dan atau janji-janji yang sifat dan atau isinya serupa kepada pihak lain, selain kepada <b>PENYELENGGARA</b>.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 21<br>
                    ALAMAT PIHAK-PIHAK<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                Seluruh pembayaran Hutang atau setiap bagian dari Hutang <b>PENERIMA PEMBIAYAAN</b> dan surat menyurat harus dilakukan/dialamatkan pada Kantor <b>PENYELENGGARA</b> yang telah ditentukan pada jam-jam kerja dari Kantor yang bersangkutan.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Semua surat menyurat dan pernyataan tertulis yang timbul dari dan bersumber pada Akad ini dianggap telah diserahkan dan diterima apabila dikirimkan kepada:
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">a.</td>
            <td class="kanankiri">
                Pihak <b>PENYELENGGARA</b> dengan alamat Kantor <b>PENYELENGGARA</b> yang bersangkutan.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">b.</td>
            <td class="kanankiri">
                <b>PENERIMA PEMBIAYAAN</b> dengan alamat barang atau alamat Kantor <b>PENERIMA PEMBIAYAAN</b> yang tercantum pada formulir permohonan Pembiayaan atau alamat yang tercantum pada Akad ini.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">3.</td>
            <td colspan="2" class="kanankiri">
                Kedua belah pihak masing-masing akan memberitahukan secara tertulis pada kesempatan pertama/secepatnya setiap terjadi perubahan alamat, <b>PENERIMA PEMBIAYAAN</b> pindah/tidak lagi menghuni barang yang bersangkutan dan sebagainya.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 22<br>
                    HUKUM YANG BERLAKU<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                Pelaksanaan Akad ini tunduk kepada ketentuan perundang-undangan yang berlaku di Indonesia dan ketentuan Syariah yang berlaku bagi <b>PENYELENGGARA.</b>
            </td>
        </tr>
    </tbody>
</table>