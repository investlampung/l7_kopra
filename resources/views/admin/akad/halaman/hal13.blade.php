<table width="100%">
    <tbody>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    LAMPIRAN<br>
                    JADWAL PEMBAYARAN ANGSURAN<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td width="30%">Harga Beli</td>
            @php
            $lamp_a = terbilang($data->lamp_ang_a);
            @endphp
            <td>: Rp. {{ number_format($data->lamp_ang_a, 0, ".", ".")}},- ({{ucwords($lamp_a)}})</td>
        </tr>
        <tr>
            <td>Marjin Keuntungan</td>
            @php
            $lamp_b = terbilang($data->lamp_ang_b);
            @endphp
            <td>: Rp. {{ number_format($data->lamp_ang_b, 0, ".", ".")}},- ({{ucwords($lamp_b)}})</td>
        </tr>
        <tr>
            <td>Harga Jual</td>
            @php
            $lamp_c = terbilang($data->lamp_ang_c);
            @endphp
            <td>: Rp. {{ number_format($data->lamp_ang_c, 0, ".", ".")}},- ({{ucwords($lamp_c)}})</td>
        </tr>
        <tr>
            <td>Angsuran per bulan</td>
            @php
            $lamp_d = terbilang($data->lamp_ang_d);
            @endphp
            <td>: Rp. {{ number_format($data->lamp_ang_d, 0, ".", ".")}},- ({{ucwords($lamp_d)}})</td>
        </tr>
    </tbody>
</table>
<table border="1" width="100%" style="margin-top: 30px;">
    <thead>
        <tr>
            <th style="padding-left: 10px;">Tanggal Pembayaran</th>
            <th style="padding-left: 10px;">Jumlah Pembayaran Angsuran</th>
        </tr>
    </thead>
    <tbody>

        @foreach($data->akadbarang as $item)
        <tr>
            <td style="padding-left: 10px;">{{ tanggal_local($item->tanggal) }}</td>
            <td style="padding-left: 10px;">Rp. {{ number_format($item->jml, 0, ".", ".")}},-</td>
        </tr>
        @endforeach
    </tbody>
</table>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>