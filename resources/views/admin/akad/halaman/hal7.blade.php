<table width="100%">
    <tbody>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                <b>PENERIMA PEMBIAYAAN</b> wajib memberikan bantuan sepenuhnya guna memungkinkan <b>PENYELENGGARA</b> melaksanakan pengikatan Barang yang dibiayai dengan fasilitas Pembiayaan sebagai jaminan menurut cara dan pada saat yang dianggap baik oleh <b>PENYELENGGARA</b>. Bukti Kepemilikan Barang dan Pengikatan Barang Jaminan dikuasai oleh <b>PENYELENGGARA</b> sampai seluruh jumlah Pembiayaan dilunasi.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">3.</td>
            <td colspan="2" class="kanankiri">
                Seluruh biaya dalam pengikatan Barang Jaminan menjadi tanggungan <b>PENERIMA PEMBIAYAAN</b>.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 12<br>
                    PEMELIHARAAN BARANG<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                <b>PENERIMA PEMBIAYAAN</b> wajib memelihara barang yang dibiayai dengan fasilitas Pembiayaan sesuai dengan tujuan Pembiayaan.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                <b>PENERIMA PEMBIAYAAN</b> tanpa persetujuan tertulis terlebih dahulu dari <b>PENYELENGGARA</b> dilarang untuk:
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">a.</td>
            <td colspan="2" class="kanankiri">
                Merubah bentuk atau konstruksi barang yang dijaminkan.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">b.</td>
            <td colspan="2" class="kanankiri">
                Membebani lagi barang tersebut dengan Hak Tanggungan atau dengan sesuatu jenis pembebanan lain apapun juga untuk keuntungan pihak lain kecuali <b>PENYELENGGARA</b>.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">c.</td>
            <td colspan="2" class="kanankiri">
                Menyewakan, menjual atau mengijinkan penempatan atau penggunaan maupun menguasakan harta tersebut kepada pihak lain.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">d.</td>
            <td colspan="2" class="kanankiri">
                Menyerahkan barang tersebut kepada pihak lain.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">e.</td>
            <td colspan="2" class="kanankiri">
                Menjaminkan hak penerimaan uang sewa atas harta tersebut.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">f.</td>
            <td colspan="2" class="kanankiri">
                Menerima uang muka, sewa atau sesuatu pembayaran lainnya atau pembayaran kompensasi dimuka terhadap sewa-menyewa penempatan, penjualan atau sesuatu bentuk penguasaan lainnya atas barang tersebut dari pihak lain.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 13<br>
                    PENERIMAAN PEMBIAYAAN WANPRESTASI<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                <b>PENERIMA PEMBIAYAAN</b> dinyatakan wanprestasi, apabila tidak memenuhi dengan baik kewajiban-kewajibannya atau melanggar ketentuan-ketentuan di dalam Akad ini.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Apabila <b>PENERIMA PEMBIAYAAN</b> wanprestasi, <b>PENYELENGGARA</b> berhak untuk memberikan peringatan dalam bentuk tindakan-tindakan sebagai berikut:
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">a.</td>
            <td colspan="2" class="kanankiri">
                Memberikan peringatan baik secara lisan maupun dalam bentuk pernyataan lalai/wanprestasi berupa surat atau akta lain yang sejenis yang dikirimkan ke alamat <b>PENERIMA PEMBIAYAAN</b> .
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">b.</td>
            <td colspan="2" class="kanankiri">
                Memberikan peringatan dalam bentuk pemasangan papan Peringatan (Plank), Stiker atau dengan cara apapun yang ditempelkan atau dituliskan pada jaminan Pembiayaan.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 14<br>
                    PENGAWASAN, PEMERIKSAAN DAN TINDAKAN TERHADAP BARANG JAMINAN<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                Selama <b>PENERIMA PEMBIAYAAN</b> belum melunasi seluruh Hutang Murabahah yang timbul dari Akad ini, <b>PENYELENGGARA</b> berhak melakukan pemeriksaan dan meminta keterangan- keterangan setempat yang diperlukan.
            </td>
        </tr>
    </tbody>
</table>