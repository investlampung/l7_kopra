<table width="100%">
    <tbody>
        <tr>
            <td class="tengah" colspan="3"><img src="{{asset('itlabil/images/default/bismillah.png')}}" alt="" width="200px"></td>
        </tr>
        <tr>
            <td class="tengah" colspan="3"><i>"Dengan menyebut Nama Allah Yang Maha Pengasih lagi Maha Penyayang"</i></td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    PEMBIAYAAN RUMAH<br>
                    ANTARA<br>
                    KOPERASI MITRA GRIYA INDONESIA<br>
                    DAN<br>
                    {{$data->anggota->nama}}<br>
                    Nomor. {{$data->no_janji}}<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="3">Yang bertanda tangan dibawah ini:<br><br></td>
        </tr>
        <tr>
            <td width="15px" valign="top">I.</td>
            <td class="kanankiri" colspan="2">
                <p class="kanankiri">
                    Koperasi Mitra Griya Indonesia, berkantor di Jalan satria no 147 Gadingrejo Pringsewu Lampung – Indonesia dalam hal ini melalui,
                </p>
            </td>
        </tr>
        <tr>
            <td></td>
            <td width="35%">Diwakili oleh</td>
            <td>: Wahyudi</td>
        </tr>
        <tr>
            <td></td>
            <td>Dalam Kapasitasnya selaku</td>
            <td>: Ketua</td>
        </tr>
        <tr>
            <td width="10px"></td>
            <td colspan="2">
                <p class="kanankiri">
                    Berdasarkan Perjanjian Layanan Pembiayaan Berbasis Teknologi Dengan Prinsip Syariah No. {{$data->no_syariah}}
                    Tanggal
                    @if(empty($data->tanggal_janji))
                    ......
                    @else
                    {{ tanggal_local($data->tanggal_janji) }}
                    @endif
                    dalam hal ini bertindak selaku Ketua dari <b>PEMBERI PEMBIAYAAN</b>, selanjutnya
                    disebut <b>PENYELENGGARA</b>;
                </p>
            </td>
        </tr>
        <tr>
            <td>II.</td>
            <td width="30%">Nama</td>
            <td>: {{$data->anggota->nama}}</td>
        </tr>
        <tr>
            <td></td>
            <td>Pekerjaan</td>
            <td>: {{$data->anggota->pekerjaan->pekerjaan}}</td>
        </tr>
        <tr>
            <td></td>
            <td>Alamat</td>
            <td>: {{$data->anggota->alamat}}</td>
        </tr>
        <tr>
            <td></td>
            <td>Nomor KTP</td>
            <td>: {{$data->anggota->no_ktp}}</td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">Dalam hal ini bertindak untuk diri sendiri, selanjutnya disebut <b>PENERIMA PEMBIAYAAN</b>.<br><br></td>
        </tr>
        <tr>
            <td colspan="3">
                <p class="kanankiri">
                    Bahwa <b>PENERIMA PEMBIAYAAN</b> telah mengajukan permohonan fasilitas pembiayaan kepada <b>PENYELENGGARA</b> untuk membeli Rumah (sebagaimana didefinisikan dalam Perjanjian) dan selanjutnya <b>PENYELENGGARA</b> menyetujui untuk menyediakan fasilitas pembiayaan sesuai dengan ketentuan dan syarat-syarat sebagaimana dinyatakan dalam Perjanjian.
                    <br>
                </p>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <p class="kanankiri">
                    Dengan ini kedua belah pihak telah sepakat untuk mengadakan Perjanjian Pembiayaan dengan prinsip Murabahah (selanjutnya disebut “Akad”) berdasarkan ketentuan dan syarat-syarat sebagai berikut:
                    <br><br>
                </p>
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    PASAL 1<br>
                    KETENTUAN POKOK AKAD<br>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                Ketentuan-ketentuan pokok Akad ini meliputi sebagai berikut:
            </td>
        </tr>
        <tr>
            <td>a.</td>
            <td>Harga Beli</td>
            @php
            $text_uang1 = terbilang($data->pasal1_a);
            @endphp
            <td>: Rp. {{ number_format($data->pasal1_a, 0, ".", ".")}},- ({{ucwords($text_uang1)}})</td>
        </tr>
        <tr>
            <td>b.</td>
            <td>Marjin Keuntungan</td>
            @php
            $text_uang2 = terbilang($data->pasal1_b);
            @endphp
            <td>: Rp. {{ number_format($data->pasal1_b, 0, ".", ".")}},- ({{ucwords($text_uang2)}})</td>
        </tr>
        <tr>
            <td>c.</td>
            <td>Harga Jual</td>
            @php
            $text_uang3 = terbilang($data->pasal1_c);
            @endphp
            <td>: Rp. {{ number_format($data->pasal1_c, 0, ".", ".")}},- ({{ucwords($text_uang3)}})</td>
        </tr>
    </tbody>
</table>