<table width="100%">
    <tbody>
        <tr>
            <td colspan="3" class="kanankiri">
                bulan {{$data->pasal5_c}} ( {{terbilang($data->pasal5_c)}} ) tahun {{$data->pasal5_d}} ( {{terbilang($data->pasal5_d)}} ).
            </td>
        </tr>
        <tr>
            <td colspan="3" class="kanankiri">
                <br>Berakhirnya jatuh tempo Pembiayaan tidak dengan sendirinya menyebabkan Hutang lunas sepanjang masih terdapat sisa Hutang <b>PENERIMA PEMBIAYAAN</b> .
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 6<br>
                    POTONGAN HARGA/DISKON<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="kanankiri">
                Jika <b>PENYELENGGARA</b> mendapat potongan harga dari pemasok, maka potongan itu merupakan hak <b>PENERIMA PEMBIAYAAN</b> , baik terjadi sebelum maupun sesudah akad.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 7<br>
                    PEMBAYARAN KEMBALI PEMBIAYAAN<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                <b>PENERIMA PEMBIAYAAN</b> wajib melakukan pembayaran kembali Pembiayaan secara angsuran sampai dengan seluruh Hutang Murabahah <b>PENERIMA PEMBIAYAAN</b> lunas sesuai dengan jadwal angsuran yang disepakati sebagaimana terdapat pada Lampiran [AAA].
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Dalam hal jatuh tempo pembayaran angsuran Pembiayaan Murabahah jatuh bertepatan dengan bukan pada Hari Kerja <b>PENYELENGGARA</b>, maka <b>PENERIMA PEMBIAYAAN</b> berjanji dan dengan ini mengikatkan diri untuk melakukan pembayaran pada Hari Kerja <b>PENYELENGGARA</b> berikutnya kecuali jika jatuh temponya pada akhir bulan berjalan, maka pembayarannya dilakukan pada Hari Kerja <b>PENYELENGGARA</b> sebelumnya.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">3.</td>
            <td colspan="2" class="kanankiri">
                Setiap pembayaran yang diterima oleh <b>PENYELENGGARA</b> dari <b>PENERIMA PEMBIAYAAN</b> atas kewajiban Pembiayaan dibukukan oleh <b>PENYELENGGARA</b> kedalam account <b>PENERIMA PEMBIAYAAN</b> sesuai dengan kebijakan <b>PENYELENGGARA</b> berdasarkan catatan dan pembukuan yang ada pada <b>PENYELENGGARA</b>.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">4.</td>
            <td colspan="2" class="kanankiri">
                Setiap pembayaran oleh <b>PENERIMA PEMBIAYAAN</b> kepada <b>PENYELENGGARA</b> akan digunakan untuk membayar :
            </td>
        </tr>
        <tr>
            <td width="20px"></td>
            <td width="20px">a.</td>
            <td class="kanankiri">
                pertama, melunasi pembayaran angsuran/pelunasan atas Harga Jual;
            </td>
        </tr>
        <tr>
            <td width="20px"></td>
            <td width="20px">b.</td>
            <td class="kanankiri">
                kedua, biaya ganti rugi;
            </td>
        </tr>
        <tr>
            <td width="20px"></td>
            <td width="20px">c.</td>
            <td class="kanankiri">
                ketiga, denda keterlambatan; dan
            </td>
        </tr>
        <tr>
            <td width="20px"></td>
            <td width="20px">d.</td>
            <td class="kanankiri">
                keempat, biaya-biaya lain.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">5.</td>
            <td colspan="2" class="kanankiri">
                Dalam hal <b>PENERIMA PEMBIAYAAN</b> merasa bahwa pembukuan/pencatatan <b>PENYELENGGARA</b> atas kewajiban dan pembayaran yang telah dilakukan tidak benar, <b>PENERIMA PEMBIAYAAN</b> berhak untuk mengajukan keberatan/ klaim kepada <b>PENYELENGGARA</b> dengan disertai bukti-bukti pembayaran yang sah. Namun bila <b>PENERIMA PEMBIAYAAN</b> tidak dapat menunjukkan bukti-bukti pembayaran yang sah, maka yang dianggap benar adalah catatan pembukuan <b>PENYELENGGARA</b>.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">6.</td>
            <td colspan="2" class="kanankiri">
                Sepanjang mengenai kewajiban-kewajiban pembayaran <b>PENERIMA PEMBIAYAAN</b> kepada <b>PENYELENGGARA</b> yang timbul dari Akad ini, maka <b>PENERIMA PEMBIAYAAN</b> dengan ini memberi kuasa kepada <b>PENYELENGGARA</b> untuk meminta dan menerima bagian dari gaji dan atau penerimaan lainnya yang menjadi hak <b>PENERIMA PEMBIAYAAN</b> dari pejabat yang berwenang membayarkan gaji dan atau penerimaan lainnya dari Instansi/Kantor dimana <b>PENERIMA PEMBIAYAAN</b> bekerja untuk pembayaran angsuran/Hutang <b>PENERIMA PEMBIAYAAN</b> .
            </td>
        </tr>
    </tbody>
</table>