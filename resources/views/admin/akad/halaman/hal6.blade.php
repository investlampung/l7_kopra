<table width="100%">
    <tbody>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 8<br>
                    DENDA TUNGGAKAN<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                Kewajiban angsuran yang tidak dilunasi merupakan tunggakan angsuran.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Dalam hal terjadi kelambatan pembayaran oleh <b>PENERIMA PEMBIAYAAN</b> kepada <b>PENYELENGGARA</b>, maka <b>PENERIMA PEMBIAYAAN</b> berjanji dan dengan ini mengikatkan diri untuk membayar :
            </td>
        </tr>
        <tr>
            <td width="20px"></td>
            <td valign="top" width="20px">a.</td>
            <td colspan="2" class="kanankiri">
                Gantirugi kerugian <b>PENYELENGGARA</b> dalam rangka melakukan penagihan kepada <b>PENERIMA PEMBIAYAAN</b> , meliputi tetapi tidak terbatas pada biaya komunikasi, transportasi, dan/atau akomodasi penagihan.
            </td>
        </tr>
        <tr>
            <td width="20px"></td>
            <td valign="top" width="20px">b.</td>
            <td colspan="2" class="kanankiri">
                @php
                $denda = terbilang($data->pasal8_a);
                @endphp
                Denda keterlambatan pada <b>PENYELENGGARA</b> sebesar Rp. {{ number_format($data->pasal8_a, 0, ".", ".")}},- ({{ucwords($denda)}}) untuk tiap-tiap hari kelambatan, terhitung sejak 14 hari (kalender) setelah jatuh tempo pembayaran angsuran sampai saat dimana seluruh tunggakan dilunasi. Denda keterlambatan dialokasikan oleh <b>PENYELENGGARA</b> untuk Dana Sosial.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 9<br>
                    UANG MUKA<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="kanankiri">
                <b>PENYELENGGARA</b> dapat meminta kepada <b>PENERIMA PEMBIAYAAN</b> uang muka (urbun) untuk pembelian Barang pada Akad ini dengan ketentuan sebagai berikut:
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                Uang muka tersebut menjadi bagian pelunasan Hutang <b>PENERIMA PEMBIAYAAN</b> apabila Pembiayaan Murabahah dilaksanakan.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Apabila <b>PENERIMA PEMBIAYAAN</b> membatalkan Akad ini maka uang muka dikembalikan kepada <b>PENERIMA PEMBIAYAAN</b> setelah dikurangi dengan kerugian atau biaya yang telah dikeluarkan oleh <b>PENYELENGGARA</b>, jika uang muka lebih kecil dari kerugian <b>PENYELENGGARA</b> maka <b>PENYELENGGARA</b> dapat meminta tambahan dari <b>PENERIMA PEMBIAYAAN</b> .
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 10<br>
                    PELUNASAN DIPERCEPAT<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="kanankiri">
                Menyimpang dari pembayaran angsuran, <b>PENERIMA PEMBIAYAAN</b> dapat melakukan Pelunasan Dipercepat seluruh sisa kewajiban yang belum dilunasi yang dilakukan sebelum berakhirnya jatuh tempo Pembiayaan.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 11<br>
                    JAMINAN DAN PENGIKATNYA<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                Guna menjamin pembayaran kembali Hutang Murabahah, <b>PENERIMA PEMBIAYAAN</b> wajib menyerahkan Barang yang dibiayai sebagai jaminan, serta menyerahkan bukti-bukti kepemilikan jaminan yang asli dan sah untuk diikat sesuai dengan ketentuan peraturan perundang-undangan yang berlaku.
            </td>
        </tr>
    </tbody>
</table>