<table width="100%">
    <tbody>
        <tr>
            <td width="20px" valign="top"></td>
            <td colspan="2">
                <p class="kanankiri">
                    disetujui oleh <b>PENERIMA PEMBIAYAAN</b> yang harus dibayarkan terlebih dahulu oleh <b>PENERIMA PEMBIAYAAN</b> kepada <b>PENYELENGGARA</b> sebagai salah satu syarat yang harus dipenuhi <b>PENYELENGGARA</b> untuk memperoleh Pembiayaan Murabahah dari <b>PENYELENGGARA</b>.
                </p>
            </td>
        </tr>
        <tr>
            <td valign="top">10.</td>
            <td colspan="2">
                <p class="kanankiri">
                    <b>Piutang</b> adalah hak tagih <b>PENYELENGGARA</b> kepada <b>PENERIMA PEMBIAYAAN</b> yang timbul karena <b>PENERIMA PEMBIAYAAN</b> telah menerima fasilitas pembiayaan dari <b>PENERIMA PEMBIAYAAN</b> dan besarnya adalah sama dengan Harga Jual.
                </p>
            </td>
        </tr>
        <tr>
            <td valign="top">11.</td>
            <td colspan="2">
                <p class="kanankiri">
                    <b>Hutang</b> adalah sejumlah kewajiban keuangan <b>PENERIMA PEMBIAYAAN</b> kepada <b>PENYELENGGARA</b> yang timbul dari realisasi Pembiayaan berdasarkan Akad ini, maksimal sebesar harga jual Barang.
                </p>
            </td>
        </tr>
        <tr>
            <td valign="top">12.</td>
            <td colspan="2">
                <p class="kanankiri">
                    <b>Angsuran</b> adalah sejumlah uang untuk pembayaran Jumlah Harga Jual yang wajib dibayar secara bulanan oleh <b>PENERIMA PEMBIAYAAN</b> kepada <b>PENYELENGGARA</b> sebagaimana ditentukan Akad ini.
                </p>
            </td>
        </tr>
        <tr>
            <td valign="top">13.</td>
            <td colspan="2">
                <p class="kanankiri">
                    <b>Jatuh Tempo Pembayaran Angsuran</b> adalah tanggal <b>PENERIMA PEMBIAYAAN</b> berkewajiban membayar angsuran setiap bulan.
                </p>
            </td>
        </tr>
        <tr>
            <td valign="top">14.</td>
            <td colspan="2">
                <p class="kanankiri">
                    <b>Tunggakan</b> adalah suatu Hutang Murabahah yang telah jatuh tempo, tetapi belum dibayar oleh <b>PENERIMA PEMBIAYAAN</b>.
                </p>
            </td>
        </tr>
        <tr>
            <td valign="top">15.</td>
            <td colspan="2">
                <p class="kanankiri">
                    <b>Pemasok</b> adalah pihak ketiga yang menyediakan Barang yang dibutuhkan oleh <b>PENERIMA PEMBIAYAAN</b> untuk dan atas nama <b>PENYELENGGARA</b>.
                </p>
            </td>
        </tr>
        <tr>
            <td valign="top">16.</td>
            <td colspan="2">
                <p class="kanankiri">
                    <b>Jaminan</b> adalah jaminan yang bersifat materiil maupun immaterial untuk mendukung keyakinan <b>PENYELENGGARA</b> atas kemampuan dan kesanggupan <b>PENERIMA PEMBIAYAAN</b> untuk melunasi Hutangnya sesuai Akad.
                </p>
            </td>
        </tr>
        <tr>
            <td valign="top">17.</td>
            <td colspan="2">
                <p class="kanankiri">
                    <b>Dokumen Jaminan</b> adalah akta-akta, surat-surat bukti kepemilikan, dan surat lainnya yang merupakan bukti hak atas barang jaminan berikut surat-surat lain yang merupakan satu kesatuan dan bagian tidak terpisah dari barang jaminan guna menjamin pemenuhan kewajiban <b>PENERIMA PEMBIAYAAN</b> kepada <b>PENYELENGGARA</b> berdasarkan Akad ini.
                </p>
            </td>
        </tr>
        <tr>
            <td valign="top">18.</td>
            <td colspan="2">
                <p class="kanankiri">
                    <b>Denda</b> adalah suatu sanksi atas adanya tunggakan, yang dinyatakan dalam jumlah tertentu.
                </p>
            </td>
        </tr>
        <tr>
            <td valign="top">19.</td>
            <td colspan="2">
                <p class="kanankiri">
                    <b>Hari Kerja</b> adalah Hari Kerja Otoritas Jasa Keuangan.
                </p>
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 3<br>
                    PELAKSANAAN<br>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="kanankiri">
                Pelaksanaan prinsip Murabahah yang berlangsung antara <b>PENYELENGGARA</b> dengan <b>PENERIMA PEMBIAYAAN</b> sebagai Penerima Fasilitas Pembiayaan dilaksanakan dan diatur menurut ketentuan- ketentuan dan persyaratan sebagai berikut :
            </td>
        </tr>
        <tr>
            <td valign="top">1.</td>
            <td colspan="2" class="kanankiri">
                <b>PENERIMA PEMBIAYAAN</b> membutuhkan Barang dengan spesifikasi sebagaimana terdapat pada Lampiran [BBB] dan meminta kepada <b>PENYELENGGARA</b> untuk memberikan fasilitas Pembiayaan Murabahah guna pembelian Barang.
            </td>
        </tr>
        <tr>
            <td valign="top">2.</td>
            <td colspan="2" class="kanankiri">
                <b>PENYELENGGARA</b> bersedia menyediakan Pembiayaan Murabahah sesuai dengan permohonan <b>PENERIMA PEMBIAYAAN</b>.
            </td>
        </tr>
    </tbody>
</table>