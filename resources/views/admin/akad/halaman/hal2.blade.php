<table width="100%">
    <tbody>
        <tr>
            <td width="15px">d.</td>
            <td width="35%">Biaya Administrasi</td>
            @php
            $text_uang4 = terbilang($data->pasal1_d);
            @endphp
            <td>: Rp. {{ number_format($data->pasal1_d, 0, ".", ".")}},- ({{ucwords($text_uang4)}})</td>
        </tr>
        <tr>
            <td>e.</td>
            <td>Kegunaan/Jenis Pembiayaan</td>
            <td>: Pembiayaan Kepemilikan {{$data->pasal1_e}}</td>
        </tr>
        <tr>
            <td>f.</td>
            <td>Jangka Waktu Pembiayaan</td>
            <td>: {{$data->pasal1_f}} bulan</td>
        </tr>
        <tr>
            <td>g.</td>
            <td>Jatuh Tempo Pembiayaan</td>
            <td>: {{$data->pasal1_g}}</td>
        </tr>
        <tr>
            <td>h.</td>
            <td>Angsuran per bulan</td>
            @php
            $text_uang5 = terbilang($data->pasal1_h);
            @endphp
            <td>: Rp. {{ number_format($data->pasal1_h, 0, ".", ".")}},- ({{ucwords($text_uang5)}})</td>
        </tr>
        <tr>
            <td>i.</td>
            <td>Jatuh Tempo Pembayaran Angsuran</td>
            <td>: {{$data->pasal1_i}}</td>
        </tr>
        <tr>
            <td>j.</td>
            <td>Jenis Jaminan</td>
            <td>: {{$data->pasal1_j}}</td>
        </tr>
        <tr>
            <td>k.</td>
            <td>Bukti Kepemilikan Jaminan</td>
            <td>: {{$data->pasal1_k}}</td>
        </tr>
        <tr>
            <td>l.</td>
            <td>Nama Pemilik Aset</td>
            <td>: {{$data->pasal1_l}}</td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 2<br>
                    KETENTUAN POKOK AKAD
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                Dalam Akad ini, yang dimaksud dengan :
            </td>
        </tr>
        <tr>
            <td valign="top">1.</td>
            <td colspan="2">
                <p class="kanankiri">
                    <b>Akad</b> adalah perjanjian tertulis tentang fasilitas Pembiayaan yang dibuat oleh <b>PENYELENGGARA</b> dan <b>PENERIMA PEMBIAYAAN</b> memuat ketentuan-ketentuan dan syarat- syarat yang disepakati, berikut perubahan-perubahan dan tambahan-tambahannya (addendum), sesuai dengan ketentuan dan perundang-undangan.
                </p>
            </td>
        </tr>
        <tr>
            <td valign="top">2.</td>
            <td colspan="2">
                <p class="kanankiri">
                    <b>PENYELENGGARA</b> adalah penyedia layanan pembiayaan berbasis teknologi dengan prinsip syariah yang menghimpunan dana dari pemberi pembiayaan dan yang menyediakan fasilitas pembiayaan kepada <b>PENERIMA PEMBIAYAAN</b> atas pembelian barang oleh <b>PENERIMA PEMBIAYAAN</b> dari Pemasok.
                </p>
            </td>
        </tr>
        <tr>
            <td valign="top">3.</td>
            <td colspan="2">
                <p class="kanankiri">
                    <b>Barang</b> adalah berupa Rumah yang dibiayai oleh <b>PENYELENGGARA</b> untuk kepentingan <b>PENERIMA PEMBIAYAAN</b>.
                </p>
            </td>
        </tr>
        <tr>
            <td valign="top">4.</td>
            <td colspan="2">
                <p class="kanankiri">
                    <b>PENERIMA PEMBIAYAAN</b> adalah penerima fasilitas pembiayaan yang berkewajiban membeli Barang sesuai yang disepakati oleh <b>PENERIMA PEMBIAYAAN</b> kepada <b>PENYELENGGARA</b>.
                </p>
            </td>
        </tr>
        <tr>
            <td valign="top">5.</td>
            <td colspan="2">
                <p class="kanankiri">
                    <b>Pembiayaan</b> adalah penyediaan uang atau tagihan yang dipersamakan dengan itu, berdasarkan persetujuan atau kesepakatan antara <b>PENYELENGGARA</b> dengan <b>PENERIMA PEMBIAYAAN</b> untuk pembelian barang yang mewajibkan <b>PENERIMA PEMBIAYAAN</b> untuk mengembalikan uang atau tagihan tersebut setelah jangka waktu tertentu dengan marjin keuntungan.
                </p>
            </td>
        </tr>
        <tr>
            <td valign="top">6.</td>
            <td colspan="2">
                <p class="kanankiri">
                    <b>Harga Beli</b> adalah sejumlah uang yang harus dibayar oleh <b>PENYELENGGARA</b> kepada Pemasok untuk membiayai pembelian barang atas permintaan <b>PENERIMA PEMBIAYAAN</b> yang disetujui oleh <b>PENYELENGGARA</b> ditambah (termasuk) biaya-biaya langsung yang dikeluarkan oleh <b>PENYELENGGARA</b> untuk membiayai Barang yang dibeli <b>PENERIMA PEMBIAYAAN</b> tersebut.
                </p>
            </td>
        </tr>
        <tr>
            <td valign="top">7.</td>
            <td colspan="2">
                <p class="kanankiri">
                    <b>Harga Jual</b> adalah harga beli ditambah marjin keuntungan <b>PENYELENGGARA</b> [WU1] yang ditetapkan oleh <b>PENYELENGGARA</b> dan disetujui/disepakati oleh <b>PENERIMA PEMBIAYAAN</b> yang merupakan jumlah Pembiayaan.
                </p>
            </td>
        </tr>
        <tr>
            <td valign="top">8.</td>
            <td colspan="2">
                <p class="kanankiri">
                    <b>Marjin Keuntungan</b> adalah jumlah uang yang wajib dibayar <b>PENERIMA PEMBIAYAAN</b> kepada <b>PENYELENGGARA</b> sebagai imbalan atas Pembiayaan yang diberikan oleh <b>PENYELENGGARA</b>, yang merupakan selisih antara Harga Jual dan Harga Beli.
                </p>
            </td>
        </tr>
        <tr>
            <td valign="top">9.</td>
            <td colspan="2">
                <p class="kanankiri">
                    <b>Uang Muka</b> adalah sejumlah uang yang besarnya ditetapkan oleh <b>PENYELENGGARA</b> dan
                </p>
            </td>
        </tr>
    </tbody>
</table>