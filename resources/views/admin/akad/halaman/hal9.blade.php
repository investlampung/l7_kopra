<table width="100%">
    <tbody>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">c.</td>
            <td colspan="2" class="kanankiri">
                Barang dipergunakan untuk hal-hal yang melanggar prinsip Syariah.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">d.</td>
            <td colspan="2" class="kanankiri">
                <b>PENERIMA PEMBIAYAAN</b> membuat atau menyebabkan atau menyetujui dilakukan atau membiarkan dilakukan suatu tindakan yang membahayakan atau dapat membahayakan, mengurangi nilai atau meniadakan jaminan atas Pembiayaan yang telah diterima.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">e.</td>
            <td colspan="2" class="kanankiri">
                Barang yang diberikan oleh <b>PENERIMA PEMBIAYAAN</b> sebagai jaminan Pembiayaan telah musnah.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">f.</td>
            <td colspan="2" class="kanankiri">
                Barang dipergunakan untuk hal-hal yang melanggar prinsip Syariah.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">g.</td>
            <td colspan="2" class="kanankiri">
                <b>PENERIMA PEMBIAYAAN</b> tidak atau lalai memperpanjang jangka waktu hak atas tanah/barang yang dijaminkan kepada <b>PENYELENGGARA</b>, sesuai dengan ketentuan yang berlaku sebelum jangka waktu hak tersebut habis.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">h.</td>
            <td colspan="2" class="kanankiri">
                Keterangan yang diberikan atau hal-hal yang disampaikan atau bukti kepemilikan atas jaminan yang diserahkan kepada <b>PENYELENGGARA</b> terbukti palsu atau <b>PENERIMA PEMBIAYAAN</b> lalai atau gagal untuk memberikan keterangan yang sesungguhnya kepada <b>PENYELENGGARA</b>.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">i.</td>
            <td colspan="2" class="kanankiri">
                <b>PENERIMA PEMBIAYAAN</b> bertindak bertentangan dengan suatu peraturan perundang- undangan yang berlaku yang mempunyai akibat penting terhadap atau mempengaruhi hubungan kerjanya dengan kantor tempat bekerja.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">j.</td>
            <td colspan="2" class="kanankiri">
                Setiap sebab atau kejadian apapun antara lain perubahan bidang moneter, keuangan atau politik nasional yang mempengaruhi kegiatan bisnis pada umumnya dan menurut pertimbangan bisnis <b>PENYELENGGARA</b> tidak mungkin lagi meneruskan fasilitas Pembiayaan yang diberikan baik sementara maupun untuk seterusnya, sehingga menjadi layak bagi <b>PENYELENGGARA</b> untuk melakukan penagihan seketika seluruh sisa Hutang guna melindungi kepentingan-kepentingannya.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">2.</td>
            <td colspan="2" class="kanankiri">
                Apabila setelah mendapat peringatan dari <b>PENYELENGGARA</b>, <b>PENERIMA PEMBIAYAAN</b> tidak dapat melunasi seluruh sisa Hutang yang seketika ditagih oleh <b>PENYELENGGARA</b>, maka <b>PENYELENGGARA</b> berhak memerintahkan kepada <b>PENERIMA PEMBIAYAAN</b> dan <b>PENERIMA PEMBIAYAAN</b> wajib untuk mengosongkan/menyerahkan barang yang telah dijaminkan oleh <b>PENERIMA PEMBIAYAAN</b> kepada <b>PENYELENGGARA</b>, selambat-lambatnya dalam jangka waktu 30 (tiga puluh) hari (kalender) terhitung sejak tanggal perintah <b>PENYELENGGARA</b>, tanpa syarat-syarat dan ganti rugi apapun juga.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">3.</td>
            <td colspan="2" class="kanankiri">
                Apabila <b>PENERIMA PEMBIAYAAN</b> ternyata tidak mengosongkan/menyerahkan barangnya dalam jangka waktu yang ditentukan dalam ayat 2 pasal ini, maka <b>PENYELENGGARA</b> berhak untuk meminta bantuan pihak yang berwenang guna mengosongkan/mengambil barang tersebut.
            </td>
        </tr>
        <tr>
            <td class="tengah" colspan="3">
                <b>
                    <br>PASAL 17<br>
                    PENGUASAAN DAN PENJUALAN (EKSEKUSI) BARANG JAMINAN<br><br>
                </b>
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px">1.</td>
            <td colspan="2" class="kanankiri">
                Apabila <b>PENERIMA PEMBIAYAAN</b> wanprestasi, maka setelah memperingatkan <b>PENERIMA PEMBIAYAAN</b> , <b>PENYELENGGARA</b> berhak untuk melakukan tindakan-tindakan sebagai berikut:
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">a.</td>
            <td colspan="2" class="kanankiri">
                Melaksanakan eksekusi terhadap barang jaminan berdasarkan ketentuan perundang- undangan yang berlaku.
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">b.</td>
            <td colspan="2" class="kanankiri">
                Melaksanakan penjualan terhadap barang jaminan berdasarkan Surat Kuasa Untuk Menjual yang dibuat oleh <b>PENERIMA PEMBIAYAAN</b> .
            </td>
        </tr>
        <tr>
            <td valign="top" width="20px"></td>
            <td valign="top" width="20px">c.</td>
            <td colspan="2" class="kanankiri">
                Menetapkan harga penjualan dengan harga yang dianggap baik oleh <b>PENYELENGGARA</b>.
            </td>
        </tr>
    </tbody>
</table>