<!DOCTYPE html>
<html>

<head>
    <title>Data Akad - {{$data->anggota->nama}}</title>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <style>
        @media print {
            @page {
                size: auto !important
            }
        }

        .samping {
            padding: 15px 15px;
        }

        body {
            font-family: 'Source Sans Pro', sans-serif;
            margin: 15px 15px;
        }

        .kanankiri {
            text-align: justify;
        }

        .tengah {
            text-align: center;
        }
    </style>
</head>

<body onload="window.print()">
    <div class="container">
        <!-- HALAMAN 1 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal1')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        <!-- HALAMAN 2 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal2')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        <!-- HALAMAN 3 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal3')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        <!-- HALAMAN 4 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal4')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        <!-- HALAMAN 5 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal5')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        <!-- HALAMAN 6 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal6')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        <!-- HALAMAN 7 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal7')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        <!-- HALAMAN 8 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal8')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        <!-- HALAMAN 9 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal9')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        <!-- HALAMAN 10 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal10')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        <!-- HALAMAN 11 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal11')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        <!-- HALAMAN 12 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal12')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        <!-- HALAMAN 13 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal13')
        @include('admin.akad.footer')
        @include('admin.akad.halbar')

        <!-- HALAMAN 14 -->
        @include('admin.akad.header')
        @include('admin.akad.halaman.hal14')
        @include('admin.akad.footer')

    </div>

</body>

</html>