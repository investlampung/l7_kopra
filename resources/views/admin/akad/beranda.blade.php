@extends('layouts.admin')

@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Akad
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('admin.akad.create') }}" class="btn btn-primary">+ Akad</a><br><br>
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Akad</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Akad ID</th>
                                <th>Kelompok</th>
                                <th>Nama</th>
                                <th>Tanggal</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->anggota->kelompok->kelompok }}</td>
                                <td>{{ $item->anggota->nama }}</td>
                                <td>{{ tanggal_local($item->created_at)}}</td>
                                <td align="center">
                                    <form action="{{ route('admin.akad.destroy',$item->id) }}" method="POST">
                                        <a class="btn btn-primary" href="{{ route('admin.akad.show',$item->id) }}" target="_blank">Cetak</a>
                                        <a class="btn btn-info" href="{{ url('admin/akadbarang/index',$item->id) }}">Pembayaran</a>
                                        <a class="btn btn-success" href="{{ route('admin.akad.edit',$item->id) }}">Ubah</a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection