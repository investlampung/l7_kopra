@extends('layouts.admin')

@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Akad
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tambah Data Akad</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.akad.store') }}" class="form-horizontal" method="POST">
                        @csrf

                        <div class="col-md-6">
                            <hr>
                            Anggota
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Nama</label>
                                <div class="col-sm-8">
                                    <select class="select2 form-control" name="anggota_id">
                                        <option value="-">-</option>
                                        @foreach($anggota as $ang)
                                        <option value="{{$ang->id}}">{{$ang->nama}}</option>
                                        @endforeach
                                    </select>
                                    <small class="text-danger">{{ $errors->first('anggota_id') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">No Perjanjian</label>

                                <div class="col-sm-8">
                                    <input type="text" name="no_janji" class="form-control">
                                    <small class="text-danger">{{ $errors->first('no_janji') }}</small>
                                </div>
                            </div>
                            <hr>
                            Syariah
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">No Syariah</label>

                                <div class="col-sm-8">
                                    <input type="text" name="no_syariah" class="form-control" placeholder="No Syariah">
                                    <small class="text-danger">{{ $errors->first('no_syariah') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tanggal Syariah</label>

                                <div class="col-sm-8">
                                    <input type="date" name="tanggal_janji" class="form-control">
                                    <small class="text-danger">{{ $errors->first('tanggal_janji') }}</small>
                                </div>
                            </div>
                            <hr>
                            Pasal 1
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Harga Beli</label>

                                <div class="col-sm-8">
                                    <input type="number" name="pasal1_a" class="form-control" placeholder="Harga Beli">
                                    <small class="text-danger">{{ $errors->first('pasal1_a') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Marjin Keuntungan</label>

                                <div class="col-sm-8">
                                    <input type="number" name="pasal1_b" class="form-control" placeholder="Marjin Keuntungan">
                                    <small class="text-danger">{{ $errors->first('pasal1_b') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Harga Jual</label>

                                <div class="col-sm-8">
                                    <input type="number" name="pasal1_c" class="form-control" placeholder="Harga Jual">
                                    <small class="text-danger">{{ $errors->first('pasal1_c') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Biaya Adm</label>

                                <div class="col-sm-8">
                                    <input type="number" name="pasal1_d" class="form-control" placeholder="Biaya Adm">
                                    <small class="text-danger">{{ $errors->first('pasal1_d') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Jenis Pembiayaan</label>

                                <div class="col-sm-8">
                                    <input type="text" name="pasal1_e" class="form-control" placeholder="Jenis Pembiayaan">
                                    <small class="text-danger">{{ $errors->first('pasal1_e') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Jangka Waktu</label>

                                <div class="col-sm-8">
                                    <input type="number" name="pasal1_f" class="form-control" placeholder="Jangka Waktu">
                                    <small class="text-danger">{{ $errors->first('pasal1_f') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Jatuh Tempo Pembiayaan</label>

                                <div class="col-sm-8">
                                    <input type="text" name="pasal1_g" class="form-control" placeholder="Jatuh Tempo Pembiayaan">
                                    <small class="text-danger">{{ $errors->first('pasal1_g') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Angsuran</label>

                                <div class="col-sm-8">
                                    <input type="number" name="pasal1_h" class="form-control" placeholder="Angsuran">
                                    <small class="text-danger">{{ $errors->first('pasal1_h') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Jatuh Tempo Angsuran</label>

                                <div class="col-sm-8">
                                    <input type="number" name="pasal1_i" class="form-control" placeholder="Jatuh Tempo Angsuran">
                                    <small class="text-danger">{{ $errors->first('pasal1_i') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Jenis Jaminan</label>

                                <div class="col-sm-8">
                                    <input type="text" name="pasal1_j" class="form-control" placeholder="Jenis Jaminan">
                                    <small class="text-danger">{{ $errors->first('pasal1_j') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Bukti Jaminan</label>

                                <div class="col-sm-8">
                                    <input type="text" name="pasal1_k" class="form-control" placeholder="Jaminan">
                                    <small class="text-danger">{{ $errors->first('pasal1_k') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Nama Pemilik</label>

                                <div class="col-sm-8">
                                    <input type="text" name="pasal1_l" class="form-control" placeholder="Nama Pemilik">
                                    <small class="text-danger">{{ $errors->first('pasal1_l') }}</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <hr>
                            Pasal 5
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Jangka Waktu</label>

                                <div class="col-sm-8">
                                    <input type="number" name="pasal5_a" class="form-control" placeholder="Jangka Waktu / Bulan">
                                    <small class="text-danger">{{ $errors->first('pasal5_a') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tanggal</label>

                                <div class="col-sm-8">
                                    <input type="text" name="pasal5_b" class="form-control" placeholder="Tanggal">
                                    <small class="text-danger">{{ $errors->first('pasal5_b') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Bulan</label>

                                <div class="col-sm-8">
                                    <input type="text" name="pasal5_c" class="form-control" placeholder="Bulan">
                                    <small class="text-danger">{{ $errors->first('pasal5_c') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tahun</label>

                                <div class="col-sm-8">
                                    <input type="text" name="pasal5_d" class="form-control" placeholder="Tahun">
                                    <small class="text-danger">{{ $errors->first('pasal5_d') }}</small>
                                </div>
                            </div>
                            <hr>
                            Pasal 8
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Denda Keterlambatan</label>

                                <div class="col-sm-8">
                                    <input type="number" name="pasal8_a" class="form-control" placeholder="Denda Keterlambatan">
                                    <small class="text-danger">{{ $errors->first('pasal8_a') }}</small>
                                </div>
                            </div>
                            <hr>
                            Pasal 24
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tanggal</label>

                                <div class="col-sm-8">
                                    <input type="date" name="pasal24_a" class="form-control">
                                    <small class="text-danger">{{ $errors->first('pasal24_a') }}</small>
                                </div>
                            </div>
                            <hr>
                            Jadwal Pembayaran
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Harga Beli</label>

                                <div class="col-sm-8">
                                    <input type="number" name="lamp_ang_a" class="form-control" placeholder="Harga Beli">
                                    <small class="text-danger">{{ $errors->first('lamp_ang_a') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Marjin Keuntungan</label>

                                <div class="col-sm-8">
                                    <input type="number" name="lamp_ang_b" class="form-control" placeholder="Marjin Keuntungan">
                                    <small class="text-danger">{{ $errors->first('lamp_ang_b') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Harga Jual</label>

                                <div class="col-sm-8">
                                    <input type="number" name="lamp_ang_c" class="form-control" placeholder="Harga Jual">
                                    <small class="text-danger">{{ $errors->first('lamp_ang_c') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Angsuran</label>

                                <div class="col-sm-8">
                                    <input type="number" name="lamp_ang_d" class="form-control" placeholder="Angsuran">
                                    <small class="text-danger">{{ $errors->first('lamp_ang_d') }}</small>
                                </div>
                            </div>
                            <hr>
                            Spesifikasi Rumah
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Nama Supplier</label>

                                <div class="col-sm-8">
                                    <input type="text" name="suplier_nama" class="form-control" placeholder="Nama Supplier">
                                    <small class="text-danger">{{ $errors->first('suplier_nama') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Alamat Supplier</label>

                                <div class="col-sm-8">
                                    <input type="text" name="suplier_alamat" class="form-control" placeholder="Alamat Supplier">
                                    <small class="text-danger">{{ $errors->first('suplier_alamat') }}</small>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            <a href="{{url('admin/akad')}}" class="btn btn-default  pull-right">Kembali</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.select2').select2({
        ajax: {
            url: '{{asset('admin-akad-ajax')}}',
            dataType: 'json',
            delay: 250,
            processResults: function(data) {
                return {
                    results: $.map(data, function(item) {
                        return {
                            text: item.nama,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });
</script>
@endsection