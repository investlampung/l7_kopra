<!DOCTYPE html>
<html>

<head>
    <title>Data Triwulan - {{$data->kelompok}}</title>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <style>
        @media print {
            @page {
                size: auto !important
            }
        }

        hr.new4 {
            border: 1px solid;
        }

        .samping {
            padding: 15px 15px;
        }

        body {
            font-family: 'Source Sans Pro', sans-serif;
            margin: 15px 15px;
        }

        .kanankiri {
            text-align: justify;
        }

        .tengah {
            text-align: center;
        }
    </style>
</head>

<body onload="window.print()">
    <div class="container">
        <table width="100%">
            <tr>
                <td>
                    <img src="{{asset('itlabil/images/default/logo_lap_triwulan.png')}}" alt="" width=150px>
                </td>
                <td>
                    <b>MITRA GRIYA INDONESIA</b><br>
                    <i>Alamat : Jl. Satria No. 147 Gadingrejo Kab. Pringsewu</i><br>
                    Izin Nomor AHU-0001455.AH.01.25 Tahun 2020<br>
                    Telpon (0721) 5621878 Email: mitragriya01@gmail.com website: arisanrumah.id
                </td>
            </tr>
        </table>
        <hr class="new4">
        <table>
            <tr>
                <td>Lampiran</td>
                <td>: -</td>
            </tr>
            <tr>
                <td>Perihal</td>
                <td>Angsuran Per 3 Bulan {{$data->kelompok}}</td>
            </tr>
        </table>
        <table border="1" width="100%" style="margin-top: 10px;">
            <thead>
                <tr>
                    <td rowspan="2" align="center" style="font-weight:bold">No</td>
                    <td rowspan="2" align="center" style="font-weight:bold">Nama</td>
                    <td colspan="3" align="center" style="font-weight:bold">{{$tahun}}</td>
                    <td rowspan="2" align="center" style="font-weight:bold">Keterangan</td>
                </tr>
                <tr>
                    <td align="center" style="font-weight:bold">{{$bulan_nama1}}</td>
                    <td align="center" style="font-weight:bold">{{$bulan_nama2}}</td>
                    <td align="center" style="font-weight:bold">{{$bulan_nama3}}</td>
                </tr>
            </thead>
            <tbody>
                @php
                $jml_ang_bul1=0;
                $jml_ang_bul2=0;
                $jml_ang_bul3=0;
                $jml_ang=0;
                @endphp
                @foreach($data->anggota as $item)
                <tr>
                    @php
                        $bul1 = 0;
                        $bul2 = 0;
                        $bul3 = 0;
                    @endphp
                    <td align="center">{{ $no++ }}</td>
                    <td style="padding-left: 3px;padding-right: 3px;">{{ $item->nama }}</td>
                    @foreach($item->angsuran->sortBy('bulan_id') as $ang)
                        @if($ang->bulan_id===$bulan1)
                            @php
                            $bul1 = $ang->dana;
                            $jml_ang_bul1=$jml_ang_bul1+$ang->dana;
                            @endphp
                        @elseif($ang->bulan_id===$bulan2)
                            @php
                            $bul2 = $ang->dana;
                            $jml_ang_bul2=$jml_ang_bul2+$ang->dana;
                            @endphp
                        @elseif($ang->bulan_id===$bulan3)
                            @php
                            $bul3 = $ang->dana;
                            $jml_ang_bul3=$jml_ang_bul3+$ang->dana;
                            @endphp
                        @else
                        @endif
                    @endforeach
                    <td style="padding-left: 3px;padding-right: 3px;">Rp. {{ number_format($bul1, 0, ".", ".")}}</td>
                    <td style="padding-left: 3px;padding-right: 3px;">Rp. {{ number_format($bul2, 0, ".", ".")}}</td>
                    <td style="padding-left: 3px;padding-right: 3px;">Rp. {{ number_format($bul3, 0, ".", ".")}}</td>
                    <td style="padding-left: 3px;padding-right: 3px;">{{$item->ket}}</td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="2" align="center" style="font-weight:bold">UANG MASUK</td>
                    <td style="padding-left: 3px;padding-right: 3px;font-weight:bold">Rp. {{ number_format($jml_ang_bul1, 0, ".", ".")}}</td>
                    <td style="padding-left: 3px;padding-right: 3px;font-weight:bold">Rp. {{ number_format($jml_ang_bul2, 0, ".", ".")}}</td>
                    <td style="padding-left: 3px;padding-right: 3px;font-weight:bold">Rp. {{ number_format($jml_ang_bul3, 0, ".", ".")}}</td>
                    <td style="padding-left: 3px;padding-right: 3px;font-weight:bold">Rp. {{ number_format($jml_ang, 0, ".", ".")}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</body>

</html>