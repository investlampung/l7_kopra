@extends('layouts.admin')
@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Kelompok
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Ubah Data Kelompok</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.kelompok.update', $data->id) }}" class="form-horizontal" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Kelompok</label>

                                <div class="col-sm-8">
                                    <input type="text" value="{{$data->kelompok}}" name="kelompok" class="form-control" placeholder="Kelompok / Tahap">
                                    <small class="text-danger">{{ $errors->first('kelompok') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Jumlah Setoran</label>

                                <div class="col-sm-8">
                                    <input type="number" value="{{$data->jml_setoran}}" name="jml_setoran" class="form-control" placeholder="Jumlah Setoran">
                                    <small class="text-danger">{{ $errors->first('jml_setoran') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Pembangunan Rumah</label>

                                <div class="col-sm-8">
                                    <input type="number" value="{{$data->bangun_rumah}}" name="bangun_rumah" class="form-control" placeholder="Pembangunan Rumah">
                                    <small class="text-danger">{{ $errors->first('bangun_rumah') }}</small>
                                </div>
                            </div>
                            <a href="{{url('admin/kelompok')}}" class="btn btn-default">Kembali</a>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection