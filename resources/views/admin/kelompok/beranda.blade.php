@extends('layouts.admin')

@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Kelompok
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Kelompok</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.kelompok.store') }}" class="form-horizontal" method="POST">
                        @csrf

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Kelompok</label>

                                <div class="col-sm-8">
                                    <input type="text" name="kelompok" class="form-control" placeholder="Kelompok / Tahap">
                                    <small class="text-danger">{{ $errors->first('kelompok') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Jumlah Setoran</label>

                                <div class="col-sm-8">
                                    <input type="number" name="jml_setoran" class="form-control" placeholder="Jumlah Setoran">
                                    <small class="text-danger">{{ $errors->first('jml_setoran') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Pembangunan Rumah</label>

                                <div class="col-sm-8">
                                    <input type="number" name="bangun_rumah" class="form-control" placeholder="Pembangunan Rumah">
                                    <small class="text-danger">{{ $errors->first('bangun_rumah') }}</small>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
                <hr>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID</th>
                                <th>Kelompok</th>
                                <th>Jumlah Setoran</th>
                                <th>Pembangun Rumah</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->kelompok }}</td>
                                <td>Rp. {{ number_format($item->jml_setoran, 0, ".", ".")}}</td>
                                <td>Rp. {{ number_format($item->bangun_rumah, 0, ".", ".")}}</td>
                                <td align="center">
                                    <form action="{{ route('admin.kelompok.destroy',$item->id) }}" method="POST">
                                        <a class="btn btn-success" href="{{ route('admin.kelompok.edit',$item->id) }}">Ubah</a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection