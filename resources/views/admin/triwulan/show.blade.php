<!DOCTYPE html>
<html>

<head>
    <title>Data Triwulan - {{$data->kelompok}}</title>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <style>
        @media print {
            @page {
                size: auto !important
            }
        }

        hr.new4 {
            border: 1px solid;
        }

        .samping {
            padding: 15px 15px;
        }

        body {
            font-family: 'Source Sans Pro', sans-serif;
            margin: 15px 15px;
        }

        .kanankiri {
            text-align: justify;
        }

        .tengah {
            text-align: center;
        }
    </style>
</head>

<body onload="window.print()">
    <div class="container">
        <table width="100%">
            <tr>
                <td>
                    <img src="{{asset('itlabil/images/default/logo_lap_triwulan.png')}}" alt="" width=150px>
                </td>
                <td>
                    <b>MITRA GRIYA INDONESIA</b><br>
                    <i>Alamat : Jl. Satria No. 147 Gadingrejo Kab. Pringsewu</i><br>
                    Izin Nomor AHU-0001455.AH.01.25 Tahun 2020<br>
                    Telpon (0721) 5621878 Email: mitragriya01@gmail.com website: arisanrumah.id
                </td>
            </tr>
        </table>
        <hr class="new4">
        <table>
            <tr>
                <td>Lampiran</td>
                <td>: -</td>
            </tr>
            <tr>
                <td>Perihal</td>
                <td>Angsuran Per 3 Bulan {{$data->kelompok}}</td>
            </tr>
        </table>
        <table border="1" width="100%" style="margin-top: 10px;">
            <thead>
                <tr>
                    <td rowspan="2" align="center" style="font-weight:bold">No</td>
                    <td rowspan="2" align="center" style="font-weight:bold">Nama</td>
                    <td colspan="3" align="center" style="font-weight:bold">{{$tahun}}</td>
                    <td rowspan="2" align="center" style="font-weight:bold">Keterangan</td>
                </tr>
                <tr>
                    <td align="center" style="font-weight:bold">{{$bulan_nama1}}</td>
                    <td align="center" style="font-weight:bold">{{$bulan_nama2}}</td>
                    <td align="center" style="font-weight:bold">{{$bulan_nama3}}</td>
                </tr>
            </thead>
            <tbody>
                @php
                $jml_ang_bul1=0;
                $jml_ang_bul2=0;
                $jml_ang_bul3=0;
                $jml_ang=0;
                @endphp
                @foreach($data->anggota as $item)
                <tr>
                    @php
                        $bul1 = 0;
                        $bul2 = 0;
                        $bul3 = 0;
                    @endphp
                    <td align="center">{{ $no++ }}</td>
                    <td style="padding-left: 3px;padding-right: 3px;">{{ $item->nama }}</td>
                    @foreach($item->angsuran->sortBy('bulan_id') as $ang)
                        @if($ang->bulan_id===$bulan1)
                            @php
                            $bul1 = $ang->dana;
                            $jml_ang_bul1=$jml_ang_bul1+$ang->dana;
                            @endphp
                        @elseif($ang->bulan_id===$bulan2)
                            @php
                            $bul2 = $ang->dana;
                            $jml_ang_bul2=$jml_ang_bul2+$ang->dana;
                            @endphp
                        @elseif($ang->bulan_id===$bulan3)
                            @php
                            $bul3 = $ang->dana;
                            $jml_ang_bul3=$jml_ang_bul3+$ang->dana;
                            @endphp
                        @else
                        @endif
                    @endforeach
                    <td style="padding-left: 3px;padding-right: 3px;">Rp. {{ number_format($bul1, 0, ".", ".")}}</td>
                    <td style="padding-left: 3px;padding-right: 3px;">Rp. {{ number_format($bul2, 0, ".", ".")}}</td>
                    <td style="padding-left: 3px;padding-right: 3px;">Rp. {{ number_format($bul3, 0, ".", ".")}}</td>
                    <td style="padding-left: 3px;padding-right: 3px;">{{$item->ket}}</td>
                </tr>
                @endforeach
                @php
                $jml_ang=$jml_ang_bul1+$jml_ang_bul2+$jml_ang_bul3;
                $jml_setoran = $data->jml_setoran*3;
                $tung_set1 = 0;
                $tung_set2 = 0;
                $tung_set3 = 0;

                $tung_set1 = $data->jml_setoran - $jml_ang_bul1;
                $tung_set2 = $data->jml_setoran - $jml_ang_bul2;
                $tung_set3 = $data->jml_setoran - $jml_ang_bul3;
                $jum_tung_set = 0;
                $jum_tung_set = $tung_set1+$tung_set2+$tung_set3;

                $per1 = 0;
                $per2 = 0;
                $per3 = 0;
                $per1 = ($tung_set1/$data->jml_setoran)*100;
                $per2 = ($tung_set2/$data->jml_setoran)*100;
                $per3 = ($tung_set3/$data->jml_setoran)*100;

                $per4=0;
                $per4=($jum_tung_set/$jml_setoran)*100;
                $bangun_rumah = $data->bangun_rumah;
                $sisa = abs($jml_ang-$bangun_rumah);
                @endphp
                <tr>
                    <td colspan="2" align="center" style="font-weight:bold">UANG MASUK</td>
                    <td style="padding-left: 3px;padding-right: 3px;font-weight:bold">Rp. {{ number_format($jml_ang_bul1, 0, ".", ".")}}</td>
                    <td style="padding-left: 3px;padding-right: 3px;font-weight:bold">Rp. {{ number_format($jml_ang_bul2, 0, ".", ".")}}</td>
                    <td style="padding-left: 3px;padding-right: 3px;font-weight:bold">Rp. {{ number_format($jml_ang_bul3, 0, ".", ".")}}</td>
                    <td style="padding-left: 3px;padding-right: 3px;font-weight:bold">Rp. {{ number_format($jml_ang, 0, ".", ".")}}</td>
                </tr>
                <tr>
                    <td colspan="2" align="center" style="font-weight:bold">SETORAN FULL</td>
                    <td style="padding-left: 3px;padding-right: 3px;font-weight:bold">Rp. {{ number_format($data->jml_setoran, 0, ".", ".")}}</td>
                    <td style="padding-left: 3px;padding-right: 3px;font-weight:bold">Rp. {{ number_format($data->jml_setoran, 0, ".", ".")}}</td>
                    <td style="padding-left: 3px;padding-right: 3px;font-weight:bold">Rp. {{ number_format($data->jml_setoran, 0, ".", ".")}}</td>
                    <td style="padding-left: 3px;padding-right: 3px;font-weight:bold">Rp. {{ number_format($jml_setoran, 0, ".", ".")}}</td>
                </tr>
                <tr>
                    <td colspan="2" align="center" style="font-weight:bold">TUNGGAKAN SETORAN</td>
                    <td style="padding-left: 3px;padding-right: 3px;font-weight:bold">Rp. ({{ number_format($tung_set1, 0, ".", ".")}})</td>
                    <td style="padding-left: 3px;padding-right: 3px;font-weight:bold">Rp. ({{ number_format($tung_set2, 0, ".", ".")}})</td>
                    <td style="padding-left: 3px;padding-right: 3px;font-weight:bold">Rp. ({{ number_format($tung_set3, 0, ".", ".")}})</td>
                    <td style="padding-left: 3px;padding-right: 3px;font-weight:bold">Rp. ({{ number_format($jum_tung_set, 0, ".", ".")}})</td>
                </tr>
                <tr>
                    <td colspan="2" align="center" style="font-weight:bold">PERSENTASE</td>
                    <td style="padding-left: 3px;padding-right: 3px;font-weight:bold" align="right">- {{round($per1)}}%</td>
                    <td style="padding-left: 3px;padding-right: 3px;font-weight:bold" align="right">- {{round($per2)}}%</td>
                    <td style="padding-left: 3px;padding-right: 3px;font-weight:bold" align="right">- {{round($per3)}}%</td>
                    <td style="padding-left: 3px;padding-right: 3px;font-weight:bold" align="right">- {{round($per4)}}%</td>
                </tr>
            </tbody>
        </table>
        <table width="100%" style="margin-top: 10px;margin-left:40px;">
            <tr>
                <td><b>Total Uang Masuk selama 3 bulan</b></td>
                <td></td>
            </tr>
            <tr height="30px">
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Saldo 3 Bulan</td>
                <td>Rp. {{ number_format($jml_ang, 0, ".", ".")}}</td>
            </tr>
            <tr>
                <td>Pembangunan Rumah {{$bangun}}</td>
                <td><u>Rp. {{ number_format($bangun_rumah, 0, ".", ".")}}</u></td>
            </tr>
            <tr>
                <td>Sisa Saldo </td>
                <td>Rp. {{ number_format($sisa, 0, ".", ".")}}</td>
            </tr>
        </table>
    </div>
</body>

</html>