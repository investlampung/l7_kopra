@extends('layouts.admin')

@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Triwulan
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Triwulan</h3>
                </div>
                <hr>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kelompok</th>
                                <th>Triwulan 1</th>
                                <th>Triwulan 2</th>
                                <th>Triwulan 3</th>
                                <th>Triwulan 4</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->kelompok }}</td>
                                <td align="center">
                                    <a class="btn btn-success" href="{{ url('admin/triwulan/show',[$item->id, 'tw1']) }}" target="_blank">Lihat</a>
                                </td>
                                <td align="center">
                                    <a class="btn btn-success" href="{{ url('admin/triwulan/show',[$item->id, 'tw2']) }}" target="_blank">Lihat</a>
                                </td>
                                <td align="center">
                                    <a class="btn btn-success" href="{{ url('admin/triwulan/show',[$item->id, 'tw3']) }}" target="_blank">Lihat</a>
                                </td>
                                <td align="center">
                                    <a class="btn btn-success" href="{{ url('admin/triwulan/show',[$item->id, 'tw4']) }}" target="_blank">Lihat</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection