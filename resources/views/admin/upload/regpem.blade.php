@extends('layouts.admin')

@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Upload
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Registrasi Pembiayaan</h3>
                </div>

                <div class="box-body">
                    <form action="{{ route('admin.import.regpem') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Bulan</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="bulan_id">
                                    @foreach($bulan as $bul)
                                    <option value="{{$bul->id}}">{{$bul->bulan}}</option>
                                    @endforeach
                                </select>
                                <small class="text-danger">{{ $errors->first('bulan_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="toko" class="col-sm-4 control-label">File Excel</label>
                            <div class="col-sm-8">
                                <input type="file" name="file" class="form-control">
                                Pastikan file ber extensi .xls atau .xlsx
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Unggah</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection