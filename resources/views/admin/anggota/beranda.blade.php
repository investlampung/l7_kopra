@extends('layouts.admin')

@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Anggota
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('admin.anggota.create') }}" class="btn btn-primary">+ Anggota</a><br><br>
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Anggota</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Anggota ID</th>
                                <th>Kelompok</th>
                                <th>Nama</th>
                                <th>NIK</th>
                                <th>Kd Rekening</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($data as $item)
                            @foreach($item->anggota->sortBy('nama') as $anggota)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $anggota->id }}</td>
                                <td>{{ $item->kelompok }}</td>
                                <td>{{ $anggota->nama }}</td>
                                <td>{{ $anggota->no_ktp }}</td>
                                <td>{{ $anggota->kode_rekening }}</td>
                                <td align="center">
                                    <form action="{{ route('admin.anggota.destroy',$anggota->id) }}" method="POST">
                                        <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#modal-barang{{$anggota->id }}">Lihat</a>
                                        <a class="btn btn-success" href="{{ route('admin.anggota.edit',$anggota->id) }}">Ubah</a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            <!-- Anggota -->
                            <div class="modal fade in" id="modal-barang{{$anggota->id}}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                            <h4 class="modal-title">
                                                Data {{$anggota->nama}} | {{ $item->kelompok }}
                                            </h4>
                                        </div>
                                        <div class="box-body">
                                            @php
                                            $nop=1;
                                            @endphp
                                        </div>
                                        <div class="box-body">
                                            <div class="col-md-4">
                                                NIK
                                            </div>
                                            <div class="col-md-8">
                                                : {{ $anggota->no_ktp }}
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="col-md-4">
                                                Kd Rekekning
                                            </div>
                                            <div class="col-md-8">
                                                : {{ $anggota->kode_rekening }}
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="col-md-4">
                                                Kd Pembiayaan
                                            </div>
                                            <div class="col-md-8">
                                                : {{ $anggota->kode_pembiayaan }}
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="col-md-4">
                                                Pekerjaan
                                            </div>
                                            <div class="col-md-8">
                                                : {{ $anggota->pekerjaan->pekerjaan }}
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="col-md-4">
                                                No Telp
                                            </div>
                                            <div class="col-md-8">
                                                : {{ $anggota->no_telp }}
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="col-md-4">
                                                Alamat
                                            </div>
                                            <div class="col-md-8">
                                                : {{ $anggota->alamat }}
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="col-md-4">
                                                Keterangan
                                            </div>
                                            <div class="col-md-8">
                                                : {{ $anggota->ket }}
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            <button class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection