@extends('layouts.admin')

@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Anggota
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tambah Data Anggota</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.anggota.store') }}" class="form-horizontal" method="POST">
                        @csrf

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Kelompok</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="kelompok_id">
                                        @foreach($kelompok as $kel)
                                        <option value="{{$kel->id}}">{{$kel->kelompok}}</option>
                                        @endforeach
                                    </select>
                                    <small class="text-danger">{{ $errors->first('kelompok_id') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Nama</label>

                                <div class="col-sm-8">
                                    <input type="text" name="nama" class="form-control" placeholder="Nama">
                                    <small class="text-danger">{{ $errors->first('nama') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">NIK</label>

                                <div class="col-sm-8">
                                    <input type="number" name="no_ktp" class="form-control" placeholder="NIK">
                                    <small class="text-danger">{{ $errors->first('no_ktp') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Kode Rekening</label>

                                <div class="col-sm-8">
                                    <input type="text" name="kode_rekening" class="form-control" placeholder="Kode Rekening">
                                    <small class="text-danger">{{ $errors->first('kode_rekening') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Kode Pembiayaan</label>

                                <div class="col-sm-8">
                                    <input type="text" name="kode_pembiayaan" class="form-control" placeholder="Kode Pembiayaan">
                                    <small class="text-danger">{{ $errors->first('kode_pembiayaan') }}</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Alamat</label>

                                <div class="col-sm-8">
                                    <input type="text" name="alamat" class="form-control" placeholder="Alamat">
                                    <small class="text-danger">{{ $errors->first('alamat') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Pekerjaan</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="pekerjaan_id">
                                        @foreach($pekerjaan as $pek)
                                        <option value="{{$pek->id}}">{{$pek->pekerjaan}}</option>
                                        @endforeach
                                    </select>
                                    <small class="text-danger">{{ $errors->first('pekerjaan_id') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">No Telp</label>

                                <div class="col-sm-8">
                                    <input type="text" name="no_telp" class="form-control" placeholder="No Telp">
                                    <small class="text-danger">{{ $errors->first('no_telp') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Keterangan</label>

                                <div class="col-sm-8">
                                    <input type="text" name="ket" class="form-control" placeholder="Keterangan">
                                    <small class="text-danger">{{ $errors->first('ket') }}</small>
                                </div>
                            </div>
                            
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            <a href="{{url('admin/anggota')}}" class="btn btn-default  pull-right">Kembali</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection