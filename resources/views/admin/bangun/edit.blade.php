@extends('layouts.admin')
@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Bangun Rumah
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Ubah Data Bangun Rumah</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.bangunrumah.update', $data->id) }}" class="form-horizontal" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Anggota</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="anggota_id">
                                        <option value="{{$data->anggota_id}}">{{$data->anggota->kelompok->kelompok}} | {{$data->anggota->nama}}</option>
                                        @foreach($kelompok as $kel)
                                        @foreach($kel->anggota->sortBy('nama') as $anggota)
                                        <option value="{{$anggota->id}}">{{$kel->kelompok}} | {{$anggota->nama}}</option>
                                        @endforeach
                                        @endforeach
                                    </select>
                                    <small class="text-danger">{{ $errors->first('anggota_id') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Triwulan</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="tw">
                                        <option value="{{$data->tw}}">
                                            @if($data->tw==='tw1')
                                            Triwulan 1
                                            @elseif($data->tw==='tw2')
                                            Triwulan 2
                                            @elseif($data->tw==='tw3')
                                            Triwulan 3
                                            @elseif($data->tw==='tw4')
                                            Triwulan 4
                                            @else
                                            @endif
                                        </option>
                                        <option value="tw1">Triwulan 1</option>
                                        <option value="tw2">Triwulan 2</option>
                                        <option value="tw3">Triwulan 3</option>
                                        <option value="tw4">Triwulan 4</option>
                                    </select>
                                    <small class="text-danger">{{ $errors->first('tw') }}</small>
                                </div>
                            </div>

                            <a href="{{url('admin/bangunrumah')}}" class="btn btn-default">Kembali</a>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection