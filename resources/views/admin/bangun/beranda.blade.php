@extends('layouts.admin')

@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Bangun Rumah
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Bangun Rumah</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.bangunrumah.store') }}" class="form-horizontal" method="POST">
                        @csrf

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Anggota</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="anggota_id">
                                        @foreach($kelompok as $kel)
                                        @foreach($kel->anggota->sortBy('nama') as $anggota)
                                        <option value="{{$anggota->id}}">{{$kel->kelompok}} | {{$anggota->nama}}</option>
                                        @endforeach
                                        @endforeach
                                    </select>
                                    <small class="text-danger">{{ $errors->first('anggota_id') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Triwulan</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="tw">
                                        <option value="tw1">Triwulan 1</option>
                                        <option value="tw2">Triwulan 2</option>
                                        <option value="tw3">Triwulan 3</option>
                                        <option value="tw4">Triwulan 4</option>
                                    </select>
                                    <small class="text-danger">{{ $errors->first('tw') }}</small>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
                <hr>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kelompok</th>
                                <th>Triwulan</th>
                                <th>Nama</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->anggota->kelompok->kelompok }}</td>
                                <td>
                                    @if($item->tw==='tw1')
                                    Triwulan 1
                                    @elseif($item->tw==='tw2')
                                    Triwulan 2
                                    @elseif($item->tw==='tw3')
                                    Triwulan 3
                                    @elseif($item->tw==='tw4')
                                    Triwulan 4
                                    @else
                                    @endif
                                </td>
                                <td>{{ $item->anggota->nama }}</td>
                                <td align="center">
                                    <form action="{{ route('admin.bangunrumah.destroy',$item->id) }}" method="POST">
                                        <a class="btn btn-success" href="{{ route('admin.bangunrumah.edit',$item->id) }}">Ubah</a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection