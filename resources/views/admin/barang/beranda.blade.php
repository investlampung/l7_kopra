@extends('layouts.admin')

@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Barang
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Barang</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.barang.store') }}" class="form-horizontal" method="POST">
                        @csrf

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Barang</label>

                                <div class="col-sm-8">
                                    <input type="text" name="barang" class="form-control" placeholder="Barang">
                                    <small class="text-danger">{{ $errors->first('barang') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Harga</label>

                                <div class="col-sm-8">
                                    <input type="text" name="harga" class="form-control" placeholder="Harga">
                                    <small class="text-danger">{{ $errors->first('harga') }}</small>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
                <hr>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Barang</th>
                                <th>Harga</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->barang }}</td>
                                <td>{{ $item->harga }}</td>
                                <td align="center">
                                    <form action="{{ route('admin.barang.destroy',$item->id) }}" method="POST">
                                        <a class="btn btn-success" href="{{ route('admin.barang.edit',$item->id) }}">Ubah</a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection