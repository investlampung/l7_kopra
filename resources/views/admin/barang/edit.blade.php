@extends('layouts.admin')
@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Barang
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Ubah Data Barang</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.barang.update', $data->id) }}" class="form-horizontal" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Barang</label>

                                <div class="col-sm-8">
                                    <input type="text" value="{{$data->barang}}" name="barang" class="form-control" placeholder="Barang">
                                    <small class="text-danger">{{ $errors->first('barang') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Harga</label>

                                <div class="col-sm-8">
                                    <input type="text" value="{{$data->harga}}" name="harga" class="form-control" placeholder="Harga">
                                    <small class="text-danger">{{ $errors->first('harga') }}</small>
                                </div>
                            </div>

                            <a href="{{url('admin/barang')}}" class="btn btn-default">Kembali</a>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection