<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAkadBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('akad_barangs', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('akad_id')->unsigned();
            $table->foreign('akad_id')->references('id')->on('akads')->onDelete('cascade');
            $table->string('tanggal');
            $table->integer('jml');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('akad_barangs');
    }
}
