<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAkadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('akads', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('anggota_id')->unsigned();
            $table->foreign('anggota_id')->references('id')->on('anggotas')->onDelete('cascade');
            $table->string('no_janji')->nullable();
            $table->string('no_syariah')->nullable();
            $table->string('tanggal_janji')->nullable();
            $table->integer('pasal1_a')->nullable();
            $table->integer('pasal1_b')->nullable();
            $table->integer('pasal1_c')->nullable();
            $table->integer('pasal1_d')->nullable();
            $table->string('pasal1_e')->nullable();
            $table->integer('pasal1_f')->nullable();
            $table->string('pasal1_g')->nullable();
            $table->integer('pasal1_h')->nullable();
            $table->integer('pasal1_i')->nullable();
            $table->string('pasal1_j')->nullable();
            $table->string('pasal1_k')->nullable();
            $table->string('pasal1_l')->nullable();
            $table->string('pasal5_a')->nullable();
            $table->string('pasal5_b')->nullable();
            $table->string('pasal5_c')->nullable();
            $table->string('pasal5_d')->nullable();
            $table->integer('pasal8_a')->nullable();
            $table->string('pasal24_a')->nullable();
            $table->integer('lamp_ang_a')->nullable();
            $table->integer('lamp_ang_b')->nullable();
            $table->integer('lamp_ang_c')->nullable();
            $table->integer('lamp_ang_d')->nullable();
            $table->string('suplier_nama')->nullable();
            $table->string('suplier_alamat')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('akads');
    }
}
