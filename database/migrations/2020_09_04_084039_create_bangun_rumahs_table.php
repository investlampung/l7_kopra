<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBangunRumahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bangun_rumahs', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('tahun_id')->unsigned();
            $table->foreign('tahun_id')->references('id')->on('tahuns')->onDelete('cascade');
            $table->bigInteger('anggota_id')->unsigned();
            $table->foreign('anggota_id')->references('id')->on('anggotas')->onDelete('cascade');
            $table->string('tw');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bangun_rumahs');
    }
}
