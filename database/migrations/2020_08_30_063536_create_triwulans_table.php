<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTriwulansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('triwulans', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('tahun_id')->unsigned();
            $table->foreign('tahun_id')->references('id')->on('tahuns')->onDelete('cascade');
            $table->bigInteger('kelompok_id')->unsigned();
            $table->foreign('kelompok_id')->references('id')->on('kelompoks')->onDelete('cascade');
            $table->text('triwulan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('triwulans');
    }
}
