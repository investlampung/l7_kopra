<?php

use Illuminate\Database\Seeder;

class PekerjaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Pekerjaan::create([
            'pekerjaan' => 'Tidak Bekerja'
        ]);
        App\Pekerjaan::create([
            'pekerjaan' => 'Kuliah'
        ]);
        App\Pekerjaan::create([
            'pekerjaan' => 'Nelayan'
        ]);
        App\Pekerjaan::create([
            'pekerjaan' => 'Petani'
        ]);
        App\Pekerjaan::create([
            'pekerjaan' => 'Peternak'
        ]);
        App\Pekerjaan::create([
            'pekerjaan' => 'PNS'
        ]);
        App\Pekerjaan::create([
            'pekerjaan' => 'TNI'
        ]);
        App\Pekerjaan::create([
            'pekerjaan' => 'Polri'
        ]);
        App\Pekerjaan::create([
            'pekerjaan' => 'Karyawan Swasta'
        ]);
        App\Pekerjaan::create([
            'pekerjaan' => 'Pedagang Kecil'
        ]);
        App\Pekerjaan::create([
            'pekerjaan' => 'Pedagang Besar'
        ]);
        App\Pekerjaan::create([
            'pekerjaan' => 'Wiraswasta'
        ]);
        App\Pekerjaan::create([
            'pekerjaan' => 'Wirausaha'
        ]);
        App\Pekerjaan::create([
            'pekerjaan' => 'Buruh'
        ]);
        App\Pekerjaan::create([
            'pekerjaan' => 'Pensiunan'
        ]);
        App\Pekerjaan::create([
            'pekerjaan' => 'Lainnya'
        ]);
    }
}
