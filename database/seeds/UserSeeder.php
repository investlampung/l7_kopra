<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'email' => 'admin@sourcemedia.id',
            'name' => 'Administrator',
            'password' => bcrypt('1sampai9')
        ]);
        App\User::create([
            'email' => 'admin@arisanrumah.id',
            'name' => 'Admin AR',
            'password' => bcrypt('1sampai2')
        ]);
    }
}
