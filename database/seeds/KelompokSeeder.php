<?php

use Illuminate\Database\Seeder;

class KelompokSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Kelompok::create(['kelompok' => 'Kelompok 1', 'jml_setoran' => 11830000, 'bangun_rumah' => 30000000]);
        App\Kelompok::create(['kelompok' => 'Kelompok 2', 'jml_setoran' => 11830000, 'bangun_rumah' => 25000000]);
        App\Kelompok::create(['kelompok' => 'Kelompok 3', 'jml_setoran' => 11830000, 'bangun_rumah' => 25000000]);
        App\Kelompok::create(['kelompok' => 'Kelompok 4', 'jml_setoran' => 11830000, 'bangun_rumah' => 25000000]);
        App\Kelompok::create(['kelompok' => 'Kelompok 5', 'jml_setoran' => 11830000, 'bangun_rumah' => 25000000]);
        App\Kelompok::create(['kelompok' => 'Kelompok 6', 'jml_setoran' => 11830000, 'bangun_rumah' => 25000000]);
        App\Kelompok::create(['kelompok' => 'Kelompok 7', 'jml_setoran' => 11830000, 'bangun_rumah' => 30000000]);
        App\Kelompok::create(['kelompok' => 'Kelompok 8', 'jml_setoran' => 11830000, 'bangun_rumah' => 30000000]);
        App\Kelompok::create(['kelompok' => 'Kelompok 9', 'jml_setoran' => 11830000, 'bangun_rumah' => 30000000]);
        App\Kelompok::create(['kelompok' => 'Kelompok 10', 'jml_setoran' => 11830000, 'bangun_rumah' => 30000000]);
        App\Kelompok::create(['kelompok' => 'Kelompok 11', 'jml_setoran' => 11830000, 'bangun_rumah' => 30000000]);
    }
}
