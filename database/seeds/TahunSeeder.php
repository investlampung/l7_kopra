<?php

use Illuminate\Database\Seeder;

class TahunSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $j=1;
        for ($i=2020; $i < 2025; $i++) { 
            App\Tahun::create(['tahun' => $i]);
            App\Bulan::create(['tahun_id'=>$j,'bulan' => 'Januari']);
            App\Bulan::create(['tahun_id'=>$j,'bulan' => 'Februari']);
            App\Bulan::create(['tahun_id'=>$j,'bulan' => 'Maret']);
            App\Bulan::create(['tahun_id'=>$j,'bulan' => 'April']);
            App\Bulan::create(['tahun_id'=>$j,'bulan' => 'Mei']);
            App\Bulan::create(['tahun_id'=>$j,'bulan' => 'Juni']);
            App\Bulan::create(['tahun_id'=>$j,'bulan' => 'Juli']);
            App\Bulan::create(['tahun_id'=>$j,'bulan' => 'Agustus']);
            App\Bulan::create(['tahun_id'=>$j,'bulan' => 'September']);
            App\Bulan::create(['tahun_id'=>$j,'bulan' => 'Oktober']);
            App\Bulan::create(['tahun_id'=>$j,'bulan' => 'November']);
            App\Bulan::create(['tahun_id'=>$j,'bulan' => 'Desember']);
            $j++;
        }
    }
}
