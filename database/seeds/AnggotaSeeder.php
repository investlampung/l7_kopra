<?php

use Illuminate\Database\Seeder;

class AnggotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Anggota::create([
            'kelompok_id' => 1,
            'pekerjaan_id' => 14,
            'nama' => 'Fauzi Thoha',
            'kode_rekening' => '11100-00001-0000002',
            'kode_pembiayaan' => '',
            'alamat' => 'Lampung Tengah',
            'no_ktp' => '109825653267235',
            'no_telp' => '089643125367',
        ]);
    }
}
