<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(TahunSeeder::class);
        $this->call(KelompokSeeder::class);
        $this->call(PekerjaanSeeder::class);
        // $this->call(AnggotaSeeder::class);
    }
}
